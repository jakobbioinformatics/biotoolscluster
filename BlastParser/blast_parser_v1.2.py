#!/usr/bin/python3

version = "v1.2"
release_date = "16-06-08"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Utilities for parsing BLAST output." \
    .format(author, release_date, version)

import argparse

import bp_modules.beautify as beautify


def beautify_main(args):

    beautify.parse_blast_extended_tab_output(args.input,
                                             args.output,
                                             args.blast_db_type,
                                             args.description_col,
                                             args.dereplicate_query)


def parse_arguments():

    def default_func(args):
        print("Must specify a sub-parser, exitting...")
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: blast link')

    blast_parser = subparsers.add_parser('beautify')
    blast_parser.set_defaults(func=beautify_main)
    blast_parser.add_argument('--input', help='Input raw blast output',
                              required=True)
    blast_parser.add_argument('--blast_db_type', help='What BLAST database',
                              choices=['sprot', 'tair'], required=True)
    blast_parser.add_argument('--description_col', help='Column with description, counts from one',
                              required=True, type=int)
    blast_parser.add_argument('--output', help='Formatted blast output',
                              required=True)
    blast_parser.add_argument('--dereplicate_query',
                              help='Allow only one hit per query, gives error if annotations are different',
                              action='store_true')
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_arguments()
