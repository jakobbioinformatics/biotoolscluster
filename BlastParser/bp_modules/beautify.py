__author__ = 'jakob'


def parse_blast_extended_tab_output(input_fp, output_fp, db_type, descr_col, dereplicate_query=False):

    """Re-parses description column in target tab-delimited blast output"""

    with open(input_fp) as in_fh:

        parse_entries = list()

        for line in in_fh:
            line = line.rstrip()

            if not dereplicate_query:
                parse_entry = ParseEntry(line, descr_col, db_type)
                parse_entries.append(parse_entry)
            else:

                if len(parse_entries) > 0 and parse_entries[-1].is_same_query(line):
                    parse_entries[-1].add_line(line)
                else:
                    parse_entry = ParseEntry(line, descr_col, db_type)
                    parse_entries.append(parse_entry)

    with open(output_fp, 'w') as out_fh:
        for entry in parse_entries:
            print(entry, file=out_fh)


class ParseEntry:

    """Information and logic related to one query"""

    def __init__(self, line, descr_col, db_type):

        self.descr_col = descr_col
        self.db_type = db_type

        self.line_fields = line.split('\t')
        self.query_field = self.line_fields[0]
        self.target_field_nbr = descr_col - 1
        self.raw_descr = self.line_fields[self.target_field_nbr]
        parsed_description = parse_description_field(self.raw_descr, db_type)

        self.parsed_descriptions = list()
        self.parsed_descriptions.append(parsed_description)

    def is_same_query(self, line):
        new_line_fields = line.split('\t')
        new_query_field = new_line_fields[0]
        return self.query_field == new_query_field

    def is_same_description(self, line):
        new_descr = self.get_descr_from_line(line, is_raw=True)
        return self.raw_descr == new_descr

    def add_line(self, line):
        parsed_descr = self.get_descr_from_line(line)
        self.parsed_descriptions.append(parsed_descr)

    def get_descr_from_line(self, line, is_raw=False):
        line_fields = line.split('\t')
        descr = line_fields[self.target_field_nbr]

        if not is_raw:
            parsed_descr = parse_description_field(descr, self.db_type)
        else:
            parsed_descr = descr

        return parsed_descr

    def __str__(self):
        full_description = ' | '.join(set(self.parsed_descriptions))

        if len(set(self.parsed_descriptions)) > 1:
            print('Warning: {} has multiple annotations'.format(self.query_field))

        output_fields = self.line_fields
        output_fields[self.target_field_nbr] = full_description
        return '\t'.join(output_fields)


def parse_description_field(description, db_type):

    if db_type == 'tair':
        return parse_tair_description(description)
    elif db_type == 'sprot':
        return parse_sprot_description(description)
    else:
        raise Exception('Unknown db type: {}'.format(db_type))


def parse_tair_description(description):

    """Parse a description recevied from the TAIR database"""

    pipe_fields = description.split('|')
    descr_field = pipe_fields[2].strip()

    if descr_field == '':
        descr_field = 'NA'

    return descr_field


def parse_sprot_description(description):

    """Parse a description received from the Swiss-prot database"""

    subject_trimmed = ' '.join(description.split(' ')[1:])
    end_info_trimmed = subject_trimmed.split('OS')[0].strip()

    return end_info_trimmed
