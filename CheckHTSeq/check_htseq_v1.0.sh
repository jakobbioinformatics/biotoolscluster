#!/bin/bash

tab_out="tab"

target=${1}
mode=${2}

if [[ -z ${target} ]]; then
    echo "Usage: ${0} <target> [${tab_out}]"
fi

if [[ ${mode} != ${tab_out} ]]; then
    echo "Investigating ${target}"
fi

included_reads=$(grep -v "^__" ${target} | awk '{sum+=$2} END {print sum}')
excluded_reads=$(grep "^__" ${target} | awk '{sum+=$2} END {print sum}')
fraction=$(awk "BEGIN {print ${included_reads} / (${included_reads} + ${excluded_reads})}")

if [[ ${mode} != ${tab_out} ]]; then
    echo "--- Included reads"
    echo ${included_reads}
    echo "--- Non-matched"
    grep "^__" ${target}
else
    echo -e "${target}\t${included_reads}\t${excluded_reads}\t${fraction}"
fi
