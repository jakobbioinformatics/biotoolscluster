#!/usr/bin/python3

import argparse
from Bio import SeqIO
import statistics

version = "v1.0"
release_date = "16-05-23"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Generate length bias table based on fasta-file with all transcripts." \
    .format(author, release_date, version)


def main():

    args = parse_arguments()

    records = list(SeqIO.parse(args.in_transcripts_fasta, 'fasta'))
    gene_lengths = get_length_dict(records)
    output_length_table(args.out_length_table, gene_lengths)


def get_length_dict(records):

    """Iterate through BioSeq entries and return dict linking IDs to lists of transcript lengths"""

    gene_lengths = dict()

    for rec in records:
        name = rec.name
        transcript_length = len(rec)

        if not gene_lengths.get(name):
            gene_lengths[name] = [transcript_length]
        else:
            gene_lengths[name].append(transcript_length)

    return gene_lengths


def output_length_table(out_length_table_fp, length_dict):

    """Writes the ID (sorted) together with transcript median length"""

    with open(out_length_table_fp, 'w') as out_fh:
        for seq_id in sorted(length_dict):
            median_transcript_length = statistics.median(length_dict[seq_id])
            print("ID: {} Values: {} Median: {}".format(seq_id, length_dict[seq_id], median_transcript_length))
            output_line = '{}\t{}'.format(seq_id, float(median_transcript_length))
            print(output_line, file=out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--in_transcripts_fasta',
                        help='FASTA with all target transcripts. Note: The DE ID is expected, and could be entered'
                             'multiple times',
                        required=True)

    parser.add_argument('--out_length_table',
                        help='Two-column table containing median length per gene',
                        required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
