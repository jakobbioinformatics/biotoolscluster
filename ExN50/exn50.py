#!/usr/bin/env python3

import argparse
import subprocess
from Bio import SeqIO
import matplotlib.pyplot as plt

# import pysam <- Alternative to current subprocess + samtools

version = "v1.1"
release_date = "16-09-29"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Visualize EXN50 for BAM file and assembly, and filter on expression." \
    .format(author, release_date, version)

VIEW_SAM_COMMAND = 'samtools view -@ {threads} {input}'


def main():

    args = parse_arguments()

    contig_ids = get_contig_ids(args.assembly)

    expression_dict = get_contig_map_counts(args.bam, contig_ids, args.threads)
    length_dict = get_contig_lengths(args.assembly)

    print('Length dict: {}'.format(len(length_dict)))

    hits = 10
    for top_hit in sorted(expression_dict, key=lambda x: expression_dict[x], reverse=True):
        print('{}\t{}\t{}'.format(top_hit, expression_dict[top_hit], length_dict[top_hit]))

        hits -= 1
        if hits <= 0:
            break

    value_pairs = calculate_numbers(expression_dict, length_dict)

    if args.plot:
        print('Generating plot to: {}'.format(args.plot))
        generate_visualization(value_pairs, args.plot)

    if args.high_expression_assembly:
        output_high_expression_assembly(args.assembly, args.high_expression_assembly,
                                        expression_dict, args.expression_thres)


def output_high_expression_assembly(assembly_in_fp, assembly_out_fp, expression_dict, expression_thres):

    """Produce reduced assembly based on expression"""

    fasta_sequences = SeqIO.parse(open(assembly_in_fp), 'fasta')

    tot_expression = sum(expression_dict.values())
    retained_entries = list()
    tot_entries = 0

    for entry in fasta_sequences:

        tot_entries += 1

        parsed_id = entry.id.split()[0]

        expression_frac = expression_dict[parsed_id] / tot_expression
        if expression_frac >= expression_thres:
            retained_entries.append(entry)

    with open(assembly_out_fp, 'w') as out_fh:
        SeqIO.write(retained_entries, out_fh, 'fasta')
        print('{} of {} entries written to {}'
              .format(len(retained_entries), tot_entries, assembly_out_fp))


def get_contig_ids(assembly_fp, id_splitter=' '):

    """Retrieve contig IDs represented in assembly"""

    valid_ids = set()
    with open(assembly_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip().split(id_splitter)[0]

            if line.startswith('>'):
                valid_ids.add(line[1:])

    return valid_ids


def get_contig_map_counts(bam_fp, assembly_contig_ids, threads=1):

    """Parse BAM file and return read map count per gene"""

    sam_command_instance = VIEW_SAM_COMMAND.format(threads=threads, input=bam_fp).split(' ')
    p = subprocess.Popen(sam_command_instance, stdout=subprocess.PIPE)

    lines_parsed = 0
    show_interval = 100000

    skipped_ids = set()
    contig_map_count = dict()
    for assembly_contig_id in assembly_contig_ids:
        contig_map_count[assembly_contig_id] = 0

    for line in iter(p.stdout.readline, 'r'):

        lines_parsed += 1
        if lines_parsed % show_interval == 0:
            print('Lines parsed: {}'.format(lines_parsed))

        line = line.decode('UTF-8').rstrip()

        if line.startswith('@'):
            continue

        if line == '':
            break

        fields = line.split('\t')
        sam_contig_id = fields[2]

        if sam_contig_id not in assembly_contig_ids:
            skipped_ids.add(sam_contig_id)
            continue

        contig_map_count[sam_contig_id] += 1

    print('Number of matching IDs: {}'.format(len(contig_map_count)))
    print('Number of non-matching IDs: {}'.format(len(skipped_ids)))

    return contig_map_count


def get_contig_lengths(assembly_fp):

    contig_length_dict = dict()

    fasta_sequences = SeqIO.parse(open(assembly_fp), 'fasta')

    for entry in fasta_sequences:
        entry_id = entry.id
        entry_length = len(entry)
        contig_length_dict[entry_id] = entry_length

    return contig_length_dict


def calculate_numbers(expression_dict, length_dict):

    """Calculate list of EXN50 values and return it as value pairs"""

    value_pairs = list()

    for ex_perc in range(1, 100):
        ex_contigs = get_ex_contigs(expression_dict, ex_perc)
        contig_nx = get_contig_nx(length_dict, ex_contigs, nx=50)

        print('EX: {} N50: {}'.format(ex_perc, contig_nx))
        value_pairs.append((ex_perc, contig_nx))

    return value_pairs


def get_ex_contigs(expression_dict, expression_threshold):

    """Return contig IDs representing expression up to threshold"""

    total_expression = sum(expression_dict.values())

    expression_threshold = (total_expression * expression_threshold) / 100
    ex_contigs = list()
    summed_expression = 0

    for contig in sorted(expression_dict, key=lambda x: expression_dict[x], reverse=True):

        summed_expression += expression_dict[contig]
        ex_contigs.append(contig)
        if summed_expression > expression_threshold:
            break

    return ex_contigs


def get_contig_nx(all_contig_lengths, target_contigs, nx=50):

    """Calculate N50 for set of contigs"""

    target_lengths = list()
    for target in target_contigs:
        target_length = all_contig_lengths[target]
        target_lengths.append(target_length)

    sorted_lengths = sorted(target_lengths)
    index = len(sorted_lengths) / 2

    return sorted_lengths[int(index)]


def generate_visualization(value_pairs, output_path):

    """Generate visualization based on calculated numbers"""

    expression_list = [pair[0] for pair in value_pairs]
    n50_list = [pair[1] for pair in value_pairs]

    plt.scatter(expression_list, n50_list)
    plt.savefig(output_path, bbox_inches='tight')


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--threads', help='Number threads used by Samtools',
                        type=int, default=1)
    parser.add_argument('--plot',
                        help='Matplotlib output path. Suffix determines output format.'
                             ' No argument means no plot.')

    parser.add_argument('--bam', help='BAM alignment path', required=True)
    parser.add_argument('--assembly', help='Assembly path', required=True)

    parser.add_argument('--id_divider', help='Dividing character for unique part of ID'
                                             ' used in BAM file', default=' ')

    parser.add_argument('--high_expression_assembly',
                        help='Contigs represented by at least [expression_thres] of expression')
    parser.add_argument('--expression_thres', type=float, default=0.0001)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
