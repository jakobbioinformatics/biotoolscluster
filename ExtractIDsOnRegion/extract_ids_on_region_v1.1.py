#!/usr/bin/python3

version = "v1.1"
release_date = "16-03-10"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n" \
               "Extract gene IDs and corresponding sequences located in particular contigs."\
    .format(author, release_date, version)

import extract_ids_on_regions_modules.args_parsing as args_parsing
import extract_ids_on_regions_modules.data_import as data_import
import extract_ids_on_regions_modules.output_writer as output_writer


def main():

    args = args_parsing.parse_arguments()

    target_regions = data_import.read_input_regions(args.input_target_regions)
    all_target_gene_entries = data_import.retrieve_target_ids_from_gff3(args.input_gff3, args.gff3_feature,
                                                                        target_regions)
    output_writer.output_targeted_ids(args.output_targeted_ids, all_target_gene_entries, args.print_region)

    print('{} entries found and written to'.format(len(all_target_gene_entries)), args.output_targeted_ids)

    if args.input_fasta and args.output_fasta:
        entry_print_counter = output_writer.output_filtered_fasta(args.input_fasta, args.output_fasta,
                                                                  all_target_gene_entries, args.print_region)
        print('{} fasta-entries filtered and written to {}'.format(entry_print_counter, args.output_fasta))
    else:
        print('Options input_fasta and output_fasta not set -> Will not run fasta filtering')


if __name__ == '__main__':
    main()
