__author__ = 'jakob'

import argparse


def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('--input_gff3', required=True,
                        help='GFF3 file containing features of interest')
    parser.add_argument('--input_target_regions', required=True,
                        help='GFF3-file containing target contig IDs')
    parser.add_argument('--input_fasta',
                        help='Fasta file containing IDs in header (>[ID] [other])')
    parser.add_argument('--output_targeted_ids',
                        help='One-column list containing IDs found in targeted regions')
    parser.add_argument('--output_fasta',
                        help='Filtered fasta with headers matching targeted genes')
    parser.add_argument('--gff3_feature', default='gene',
                        help='GFF3 feature to investigate')
    parser.add_argument('--print_region', action='store_true')

    args = parser.parse_args()
    return args
