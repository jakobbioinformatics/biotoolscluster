__author__ = 'jakob'

import extract_ids_on_regions_modules.gene_entry as gene_entry


def read_input_regions(target_regions_fp):

    """
    Read the target regions from file
    Return a set containing the IDs
    """

    target_regions_temp = set()
    with open(target_regions_fp) as regions_fh:
        for region in regions_fh:
            region = region.rstrip()
            target_regions_temp.add(region)

    return target_regions_temp


def retrieve_target_ids_from_gff3(in_gff3_fp, gff3_feature, target_regions):

    """
    Read GFF3 data, extracting the IDs matching feature and region
    Return set containing all target IDs
    """

    gene_entries = dict()

    with open(in_gff3_fp) as gff3_fh:
        for line in gff3_fh:
            line = line.rstrip()

            if line.startswith('#'):
                continue

            line_fields = line.split('\t')
            line_gff3_feature = line_fields[2]
            line_region_id = line_fields[0]

            if line_gff3_feature == gff3_feature and line_region_id in target_regions:

                # Field format: ID=id_nbr;Name=name_chars;...
                field_id = line_fields[8].split(';')[0].split('=')[1]
                entry = gene_entry.GeneEntry(field_id, line_region_id)
                gene_entries[field_id] = entry

    return gene_entries
