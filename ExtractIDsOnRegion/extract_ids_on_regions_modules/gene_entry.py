__author__ = 'jakob'


class GeneEntry:

    """Entry containing related information about target gene ID"""

    def __init__(self, gene_id, region):
        self.gene_id = gene_id
        self.region = region
        self.sequence = ''

    def add_sequence(self, sequence):
        self.sequence = sequence

    def get_region_string(self, delim='\t'):
        return '{gene_id}{delim}{region}'\
            .format(gene_id=self.gene_id, delim=delim, region=self.region)
