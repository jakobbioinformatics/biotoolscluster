__author__ = 'jakob'


def output_targeted_ids(output_target_ids_fp, all_target_entries, print_region=False):

    """
    Write the found targeted IDs to a single-column txt-file
    Return number of fasta entries matching the IDs
    """

    with open(output_target_ids_fp, 'w') as out_id_fh:
        for entry_key in sorted(all_target_entries.keys()):
            entry = all_target_entries[entry_key]
            if not print_region:
                print(entry.gene_id, file=out_id_fh)
            else:
                print(entry.get_region_string(), file=out_id_fh)


def output_filtered_fasta(in_fasta_fp, out_fasta_fp, all_target_entries, print_region=False):

    """Parse through input fasta and write out entries matching targeted IDs"""

    entry_print_counter = 0

    with open(in_fasta_fp) as in_fs_fh, open(out_fasta_fp, 'w') as out_fs_fh:

        for header_line in in_fs_fh:
            header_line = header_line.rstrip()
            seq_line = in_fs_fh.readline().rstrip()

            assert header_line.startswith('>'), \
                print('File is not a fasta header, single-line format expected, but found:\n{}'
                      .format(header_line))

            # Format: >Bv1_000020_oary.t1 length=669 cDNAcoverage=77.8%
            line_id = header_line[1:].split(' ')[0].split('.')[0]
            if line_id in all_target_entries.keys():
                entry_print_counter += 1
                if not print_region:
                    print(header_line, file=out_fs_fh)
                else:
                    region = all_target_entries[line_id].region
                    print('{header_line} {region}'.format(header_line=header_line, region=region),
                          file=out_fs_fh)
                print(seq_line, file=out_fs_fh)

    return entry_print_counter
