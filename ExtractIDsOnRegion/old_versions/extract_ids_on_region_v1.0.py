#!/usr/bin/python3

version = "v1.0"
release_date = "16-01-18"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n" \
               "Extract gene IDs and corresponding sequences located in particular contigs.".format(author, release_date, version)

import argparse

def main():

    args = parse_arguments()

    target_regions = read_input_regions(args.input_target_regions)
    all_target_ids = retrieve_target_ids_from_gff3(args.input_gff3, args.gff3_feature, target_regions)
    output_targeted_ids(args.output_targeted_ids, all_target_ids)

    print('{} entries found and written to'.format(len(all_target_ids)), args.output_targeted_ids)

    if args.input_fasta and args.output_fasta:
        entry_print_counter = output_filtered_fasta(args.input_fasta, args.output_fasta, all_target_ids)
        print('{} fasta-entries filtered and written to {}'.format(entry_print_counter, args.output_fasta))
    else:
        print('Options input_fasta and output_fasta not set -> Will not run fasta filtering')


def read_input_regions(target_regions_fp):

    """
    Read the target regions from file
    Return a set containing the IDs
    """

    target_regions_temp = set()
    with open(target_regions_fp) as regions_fh:
        for region in regions_fh:
            region = region.rstrip()
            target_regions_temp.add(region)

    return target_regions_temp


def retrieve_target_ids_from_gff3(in_gff3_fp, gff3_feature, target_regions):

    """
    Read GFF3 data, extracting the IDs matching feature and region
    Return set containing all target IDs
    """

    target_ids = set()

    with open(in_gff3_fp) as gff3_fh:
        for line in gff3_fh:
            line = line.rstrip()

            if line.startswith('#'):
                continue

            line_fields = line.split('\t')
            if line_fields[2] == gff3_feature and line_fields[0] in target_regions:

                # Field format: ID=id_nbr;Name=name_chars;...
                field_id = line_fields[8].split(';')[0].split('=')[1]
                target_ids.add(field_id)

    return target_ids


def output_targeted_ids(output_target_ids_fp, all_target_ids):

    """
    Write the found targeted IDs to a single-column txt-file
    Return number of fasta entries matching the IDs
    """

    with open(output_target_ids_fp, 'w') as out_id_fh:
        for entry in all_target_ids:
            print(entry, file=out_id_fh)

def output_filtered_fasta(in_fasta_fp, out_fasta_fp, all_target_ids):

    """Parse through input fasta and write out entries matching targeted IDs"""

    entry_print_counter = 0

    with open(in_fasta_fp) as in_fs_fh, open(out_fasta_fp, 'w') as out_fs_fh:

        for header_line in in_fs_fh:
            header_line = header_line.rstrip()
            seq_line = in_fs_fh.readline().rstrip()

            assert header_line.startswith('>'), \
                print('File is not a fasta header, single-line format expected, but found:\n{}'.format(header_line))

            # Format: >Bv1_000020_oary.t1 length=669 cDNAcoverage=77.8%
            line_id = header_line[1:].split(' ')[0].split('.')[0]
            if line_id in all_target_ids:
                entry_print_counter += 1
                print(header_line, file=out_fs_fh)
                print(seq_line, file=out_fs_fh)

    return entry_print_counter

def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('--input_gff3', required=True,
                        help='GFF3 file containing features of interest')
    parser.add_argument('--input_target_regions', required=True,
                        help='GFF3-file containing target contig IDs')
    parser.add_argument('--input_fasta',
                        help='Fasta file containing IDs in header (>[ID] [other])')
    parser.add_argument('--output_targeted_ids',
                        help='One-column list containing IDs found in targeted regions')
    parser.add_argument('--output_fasta',
                        help='Filtered fasta with headers matching targeted genes')
    parser.add_argument('--gff3_feature', default='gene',
                        help='GFF3 feature to investigate')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
