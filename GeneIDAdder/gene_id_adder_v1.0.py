#!/usr/bin/python3

version = "v1.0"
release_date = "16-03-31"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Utility for inserting/replacing gene ID different file types" \
    .format(author, release_date, version)

import argparse


def main():

    args = parse_arguments()

    mapping_dict = parse_mapping_matrix_to_dict(args.mapping_matrix, args.mm_in_field_index, args.mm_out_field_index)

    if args.fasta:
        print("Parsing fasta...")
        output_parsed_fasta(args.fasta, args.output_file, mapping_dict)
    elif args.expression_matrix:
        print("Parsing expression matrix...")
        output_parsed_expression_matrix(args.expression_matrix, args.output_file, mapping_dict, args.has_header)
    else:
        print("No input file specified for parsing, will do nothing!")


def parse_mapping_matrix_to_dict(mapping_matrix_fp, in_field_index, out_field_index):

    """Parse the tab delimited mapping file, and return mapping dict"""

    mapping_dict = dict()
    with open(mapping_matrix_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            fields = line.split('\t')
            in_field = fields[in_field_index]
            out_fields = fields[out_field_index]

            mapping_dict[in_field] = out_fields
    return mapping_dict


def output_parsed_fasta(fasta_fp, out_fasta_fp, mapping_dict):

    """Output fasta with inserted ID"""

    with open(fasta_fp) as in_fs_fh, open(out_fasta_fp, 'w') as out_fs_fh:
        for line in in_fs_fh:
            line = line.rstrip()

            if line.startswith('>'):
                fasta_id = line[1:].split(' ')[0]
                target_id = mapping_dict[fasta_id]
                print('>{} {}'.format(target_id, line[1:]), file=out_fs_fh)
            else:
                print(line, file=out_fs_fh)


def output_parsed_expression_matrix(matrix_fp, out_matrix_fp, mapping_dict, has_header=False):

    """Output expression matrix with replaced ID"""

    header_parsed = False

    with open(matrix_fp) as in_fh, open(out_matrix_fp, 'w') as out_fh:
        for line in in_fh:
            line = line.rstrip()

            if has_header and not header_parsed:
                print(line, file=out_fh)
                header_parsed = True
                continue

            fields = line.split(',')
            current_id = fields[0]
            target_id = mapping_dict[current_id]
            print('{},{}'.format(target_id, ','.join(fields[1:])), file=out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--mapping_matrix', help='Tab delimited matrix with mapping information',
                        required=True)

    parser.add_argument('--fasta')
    parser.add_argument('--expression_matrix')

    parser.add_argument('--mm_in_field_index', help='Field containing type of IDs to map from', required=True, type=int)
    parser.add_argument('--mm_out_field_index', help='Field containing desired IDs', required=True, type=int)

    parser.add_argument('--has_header', action='store_true')

    parser.add_argument('--output_file', required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
