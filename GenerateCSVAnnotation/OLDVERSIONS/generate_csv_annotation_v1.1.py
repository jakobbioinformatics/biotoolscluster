#!/usr/bin/python3

version = "v1.1"
release_date = "16-02-22"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n" \
               "Read different annotation files and output relevant data in CSV format.\n" \
               "" \
               "Required input files:" \
               "" \
               "- Transcripts used for differential expression" \
               "" \
               "Possible input files for annotation:" \
               "" \
               "- Differential expression matrix" \
               "- SignalP table" \
               "- TMHMM table" \
               "- Blast annotation (in tab-delimited format)" \
               "" \
               "Possible input files for subset fastas:" \
               "" \
               "- Peptides for peptide sequence annotation" \
    .format(author, release_date, version)

import argparse
from Bio import SeqIO
from Bio.SeqIO import FastaIO

import import_functions
import entry_objects


def main():

    args = parse_arguments()
    gene_entries = parse_input_entries(args.input_transcripts, args.id_separator,
                                       args.output_delimitor, args.blast_fields)

    print('Reading annotations...')
    if args.blast:
        header_fields = args.blast_field_headers.split(',')
        value_fields = args.blast_fields.split(',')

        assert len(header_fields) == len(value_fields), \
            print('Must provide same number of blast headers and field numbers!')

    if args.de_matrix:
        import_functions.import_de_matrix(gene_entries, args.de_matrix, p_cutoff=0.05)

    if args.signalp:
        import_functions.import_signalp_annotation(gene_entries, args.signalp,
                                                   peptide_id_del=args.additional_id_del)

    if args.tmhmm:
        import_functions.import_tmhmm_annotation(gene_entries, args.tmhmm,
                                                 peptide_id_del=args.additional_id_del)

    if args.blast:
        import_functions.import_blast_annotation(gene_entries, args.blast, args.blast_fields,
                                                 peptide_id_del=args.additional_id_del)

    print('Writing table...')
    output_table(gene_entries, args.output_path, args)

    if args.output_seq_base_name:

        print('Writing sequences...')

        output_fasta_seqs(gene_entries, args.input_transcripts, args.output_seq_base_name + '.fna',
                          args.id_separator, args.output_filter_field)

        if args.peptides:
            output_fasta_seqs(gene_entries, args.peptides, args.output_seq_base_name + '.pep',
                              args.id_separator, args.output_filter_field)

    print('All done!')


def parse_input_entries(input_path, id_separator, delimit, blast_field_string):

    """Read target nucleotide fasta - The IDs are what is stored"""

    gene_entries = dict()

    for record in SeqIO.parse(input_path, 'fasta'):

        my_id = record.id.split(id_separator)[0]
        my_entry = entry_objects.GeneEntry(my_id, delimit, blast_field_string)
        gene_entries[my_id] = my_entry

    return gene_entries


def output_table(gene_entries, output_path, args, delimit=','):

    """Write matrix containing provided annotation information"""

    with open(output_path, 'w') as out_fh:

        # Print header
        header_string = 'TranscriptID'
        if args.de_matrix:
            header_string += '{0}baseMeanExpression{0}log2FoldChange{0}absFoldChange{0}adjustedPVal'.format(delimit)
        if args.signalp:
            header_string += '{0}predictedSignalPeptide'.format(delimit)
        if args.tmhmm:
            header_string += '{0}predictedTransmembranceHelices'.format(delimit)
        if args.blast:

            blast_header_fields = args.blast_field_headers.split(',')
            for field in blast_header_fields:
                header_string += '{}{}'.format(delimit, field)

        print(header_string, file=out_fh)

        # Print content
        for entry_key in gene_entries:
            target_entry = gene_entries[entry_key]
            if target_entry.get_entry(args.output_filter_field).has_value():
                print(str(target_entry), file=out_fh)


def output_fasta_seqs(gene_entries, in_fasta, output_path, fasta_id_separator, out_filter_field):

    target_gene_ids = set()
    for entry_key in gene_entries:
        target_entry = gene_entries[entry_key]
        if target_entry.get_entry(out_filter_field).has_value():
            target_gene_ids.add(entry_key)

    target_sequences = list()

    for record in SeqIO.parse(in_fasta, 'fasta'):

        my_id = record.id.split(fasta_id_separator)[0]

        if my_id in target_gene_ids:
            target_sequences.append(record)

    with open(output_path, 'w') as out_fh:
        fasta_writer = FastaIO.FastaWriter(out_fh, wrap=None)
        fasta_writer.write_file(target_sequences)


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--input_transcripts', help='Input file containing all gene/transcript sequences',
                        required=True)
    parser.add_argument('--id_separator', help='Separator in fasta header separating sequence ID from other content',
                        default=' ')

    parser.add_argument('--peptides', help='Input file containing all gene/transcript sequences')

    parser.add_argument('--output_filter_field', default='de_matrix')

    parser.add_argument('--de_matrix', help='Matrix with DE IDs, fold change, expression and significance')
    parser.add_argument('--signalp', help='SignalP information about signal peptide containing sequences')
    parser.add_argument('--tmhmm', help='TMHMM information about predicted transmembrane helices')
    parser.add_argument('--blast', help='Tab-delimited BLAST data')
    parser.add_argument('--blast_fields', help='Comma-delimited string with target blast field numbers')
    parser.add_argument('--blast_field_headers', help='Comma-delimited string with target blast field headers')

    parser.add_argument('--additional_id_del', help='Optional additional annotation ID separator')

    parser.add_argument('--output_path', required=True)
    parser.add_argument('--output_seq_base_name', help='Base name for output nucleotides and protein files')
    parser.add_argument('--output_delimitor', default=',')

    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
