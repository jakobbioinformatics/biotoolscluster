__author__ = 'jakob'

import argparse

# Input options
id_file_help = 'Input ID file, either in fasta-format or tab-delimited matrix format'
id_file_format_help = 'File format for ID file'
id_separator_help = 'Separator in fasta header separating sequence ID from other content'
output_filter_field_help = 'Specifies what data field to use for filtering of the data'
additional_id_del_help = 'Optional additional annotation ID separator'

# Other options
quiet_help = 'Do not print warning messages.'

# Fields
field_blast_help = 'Tab-delimited BLAST data. Desired BLAST data can be extracted by' \
                   'using options target_blast_columns and target_blast_columns_headers'

field_de_matrix_help = 'Matrix with DE IDs, fold change, expression and significance produced by BioRCluster'
field_peptides_help = 'Input file containing peptide sequences'
field_signalp_help = 'SignalP information about signal peptide containing sequences'

field_subset_matrix_help = 'Tab-delimited matrix with subset of IDs in column 1.' \
                           'Can provide additional information in other columns using options' \
                           'target_subset_matrix_columns and target_subset_matrix_columns_headers'

field_tmhmm_help = 'TMHMM information about predicted transmembrane helices'
field_transcripts_help = 'Input file containing gene/transcript sequences'
field_blast_linkage_help = 'Two column tsv-matrix produced by LinkGenesBetweenAnnotations software'

# Field options

# -> blast
blast_columns_help = 'Comma-delimited string with target blast field numbers'
blast_columns_headers_help = 'Comma-delimited string with target blast field headers'

# -> subset_matrix
subset_matrix_headers_help = 'Comma-delimited string with target subset matrix field headers'

# Output options
output_path_help = 'Path for written .csv output file'
output_seq_base_name_help = 'Base name for output nucleotides and protein files'
output_delimitor_help = 'Delimitor used in output matrix'


def parse_arguments(help_message, version):

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)

    # == Input options == #
    parser.add_argument('--id_file', help=id_file_help, required=True)
    parser.add_argument('--id_file_format', help=id_file_format_help, choices=['fasta', 'tsv'], default='fasta')

    # parser.add_argument('--input_ids', help=input_transcripts_help, required=True)

    parser.add_argument('--id_separator', help=id_separator_help, default=' ')
    parser.add_argument('--additional_id_del', help=additional_id_del_help)

    # == Other options == #
    parser.add_argument('--quiet', help=quiet_help, action='store_true')

    # == Field files == #
    parser.add_argument('--field_de_matrix', help=field_de_matrix_help)
    parser.add_argument('--field_peptides', help=field_peptides_help)
    parser.add_argument('--field_signalp', help=field_signalp_help)
    parser.add_argument('--field_subset_matrix', help=field_subset_matrix_help)
    parser.add_argument('--field_tmhmm', help=field_tmhmm_help)
    parser.add_argument('--field_transcripts', help=field_transcripts_help)
    parser.add_argument('--field_blast', help=field_blast_help)
    parser.add_argument('--field_blast_linkage', help=field_blast_linkage_help)

    # == Additional field options == #

    # -> blast <- #
    parser.add_argument('--blast_columns', help=blast_columns_help)
    parser.add_argument('--blast_columns_headers', help=blast_columns_headers_help)

    # -> subset_matrix <- #
    parser.add_argument('--subset_matrix_headers', help=subset_matrix_headers_help, default='ids')

    # == Output options == #
    parser.add_argument('--output_filter_field', help=output_filter_field_help, default='de_matrix')
    parser.add_argument('--output_path', help=output_path_help, required=True)
    parser.add_argument('--output_seq_base_name', help=output_seq_base_name_help)
    parser.add_argument('--output_delimitor', help=output_delimitor_help, default=',')

    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()

    return args
