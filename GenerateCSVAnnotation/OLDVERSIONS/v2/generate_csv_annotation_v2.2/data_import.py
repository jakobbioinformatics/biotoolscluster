__author__ = 'Jakob Willforss'

from Bio import SeqIO
import csv_annot_modules.entry_objects as entry_objects


def parse_input_entries(args):

    """Read target nucleotide fasta - The IDs are what is stored"""

    gene_entries = dict()

    if args.id_file_format == 'fasta':
        for record in SeqIO.parse(args.id_file, 'fasta'):
            my_id = record.id.split(args.id_separator)[0]
            my_entry = entry_objects.GeneEntry(my_id, args)
            gene_entries[my_id] = my_entry

    elif args.id_file_format == 'tsv':
        with open(args.id_file) as in_fh:
            for line in in_fh:
                line = line.rstrip()

                fields = line.split('\t')
                my_id = fields[0].split(args.id_separator)[0]
                my_entry = entry_objects.GeneEntry(my_id, args)
                gene_entries[my_id] = my_entry

    else:
        raise Exception("The provided file format '{}' is unvalid or simply not yet implemented"
                        .format(args.id_file_format))
    return gene_entries


def import_de_matrix(gene_entries, de_matrix_fp, p_cutoff, delimitor=','):

    with open(de_matrix_fp) as in_fh:

        header_entries = next(in_fh).rstrip().split(',')

        id_field = 0

        header_entries = [entry.replace('"', '') for entry in header_entries]

        base_mean_expression_field = header_entries.index('baseMean')
        fold_change_field = header_entries.index('log2FoldChange')
        adjusted_p_field = header_entries.index('padj')

        for line in in_fh:
            line = line.rstrip()
            de_matrix_fields = [entry.replace('"', '') for entry in line.split(delimitor)]

            entry_id = de_matrix_fields[id_field]
            base_mean_expression = float(de_matrix_fields[base_mean_expression_field])
            fold_change = float(de_matrix_fields[fold_change_field])
            adjusted_p = float(de_matrix_fields[adjusted_p_field])

            if adjusted_p <= p_cutoff:

                value_list = [base_mean_expression, fold_change, abs(fold_change), adjusted_p]
                gene_entries[entry_id].assign_annotation_list('de_matrix', value_list)


def import_transcripts(gene_entries, transcripts_fp, id_sep=' ', second_id_sep=None):

    """Check if IDs have provided transcript"""

    transcript_ids = set()
    for record in SeqIO.parse(transcripts_fp, 'fasta'):
        if not second_id_sep:
            my_id = record.id.split(id_sep)[0]
        else:
            my_id = record.id.split(id_sep)[0].split(second_id_sep)[0]
        transcript_ids.add(my_id)

    for transcript_id in transcript_ids:
        if transcript_id in gene_entries:
            value_list = ['Yes']
            gene_entries[transcript_id].assign_annotation_list('transcripts', value_list)


def import_peptides(gene_entries, peptides_fp, id_sep=' ', second_id_sep=None):

    """Check if IDs have provided peptides"""

    transcript_ids = set()
    for record in SeqIO.parse(peptides_fp, 'fasta'):
        if not second_id_sep:
            my_id = record.id.split(id_sep)[0]
        else:
            my_id = record.id.split(id_sep)[0].split(second_id_sep)[0]
        transcript_ids.add(my_id)

    for peptide_id in transcript_ids:
        if peptide_id in gene_entries:
            value_list = ['Yes']
            gene_entries[peptide_id].assign_annotation_list('peptides', value_list)


def import_signalp_annotation(gene_entries, signalp_path, sub_id_del=None):

    """Add information about if sequences contain identified signal peptide or not"""

    signal_peptide_ids = set()

    with open(signalp_path) as in_fh:
        for line in in_fh:

            if line.startswith('#'):
                continue

            line = line.rstrip()
            fields = line.split()

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            signal_peptide_ids.add(entry_id)

    for peptide_id in signal_peptide_ids:
        value_list = ['Yes']
        gene_entries[peptide_id].assign_annotation_list('signalp', value_list)


def import_tmhmm_annotation(gene_entries, tmhmm_path, sub_id_del=None):

    """Add information about if and how many transmembrane helices the sequences are predicted to contain"""

    with open(tmhmm_path) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split()

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            predicted_helices = int(fields[4].split('=')[1])  # Format: PredHel=0

            if predicted_helices > 0:
                value_list = [str(predicted_helices)]
                gene_entries[entry_id].assign_annotation_list('tmhmm', value_list)


def import_blast_annotation(gene_entries, blast_path, blast_field_string, sub_id_del=None):

    """Read tab delimited blast output and extract desired information by specifying the blast_field_string"""

    with open(blast_path) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            target_fields = [int(field) for field in blast_field_string.split(',')]
            target_values = [fields[target_field] for target_field in target_fields]

            gene_entries[entry_id].assign_annotation_list('blast', target_values)


def import_subset_matrix_annotation(gene_entries, subset_matrix_fp, sub_id_del=None):

    """
    Import tab delimited matrix containing subset of gene IDs in column 1
    Can contain further information in other columns, which in that case are all included
    """

    with open(subset_matrix_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            provided_values = fields[1:]  # Empty if no value columns present
            gene_entries[entry_id].assign_annotation_list('subset_matrix', provided_values)


def import_blast_linkage(gene_entries, blast_linkage_fp, sub_id_del=None):

    """
    Import two-column blast linkage file, with blast-related entries in string in second column
    linked to the target IDs in the first column
    """

    with open(blast_linkage_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            blast_linkage_string = fields[1]

            if entry_id in gene_entries:
                gene_entries[entry_id].assign_annotation_list('blast_linkage', [blast_linkage_string])
