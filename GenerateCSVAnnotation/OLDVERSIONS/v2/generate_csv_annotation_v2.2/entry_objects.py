import csv_annot_modules.utility_functions as util


class GeneEntry:

    """
    A GeneEntry represents a collection of information related to one gene or transcript present in the
    targets used for differential expression.

    The purpose of this class is to gather annotation information, optionally taking a number of additional
    annotations, and finally writing out all present annotations.
    """

    def __init__(self, fasta_id, args):

        self.options = dict()
        self.options['quiet'] = args.quiet

        self.OPTIONAL_ANNOTATIONS = ['peptides', 'transcripts', 'de_matrix', 'signalp', 'tmhmm', 'blast_linkage',
                                     'subset_matrix', 'blast']
        self.INCLUDED_ANNOTATIONS = self._get_included_annotations_list(args)

        self.fasta_id = fasta_id
        self.assigned_annotation_dict = self._setup_assigned_annotation_dict(args)
        self.out_delimit = args.output_delimitor

    @staticmethod
    def _get_included_annotations_list(args):

        """Retrieve list with assigned fields from args"""

        included_annotations = list()
        args_dict = vars(args)

        for arg in args_dict:
            if args_dict.get(arg) and arg.startswith('field_'):
                included_annotations.append(arg.replace('field_', ''))

        return included_annotations

    def _setup_assigned_annotation_dict(self, args):

        assigned_annot_dict = dict()

        assigned_annot_dict['de_matrix'] = AnnotationEntry(self.fasta_id, self.options, 'de_matrix', 2)
        assigned_annot_dict['signalp'] = AnnotationEntry(self.fasta_id, self.options, 'signalp', 1)
        assigned_annot_dict['tmhmm'] = AnnotationEntry(self.fasta_id, self.options, 'tmhmm', 1)
        assigned_annot_dict['transcripts'] = AnnotationEntry(self.fasta_id, self.options, 'transcripts', 1)
        assigned_annot_dict['peptides'] = AnnotationEntry(self.fasta_id, self.options, 'peptides', 1)
        assigned_annot_dict['blast_linkage'] = AnnotationEntry(self.fasta_id, self.options, 'blast_linkage', 1)

        if 'blast' in self.INCLUDED_ANNOTATIONS:
            nbr_blast_fields = len(args.blast_columns.split(','))
        else:
            nbr_blast_fields = 0
        assigned_annot_dict['blast'] = AnnotationEntry(self.fasta_id, self.options, 'blast', nbr_blast_fields)

        if 'subset_matrix' in self.INCLUDED_ANNOTATIONS:
            nbr_subset_matrix_fields = util.nbr_matrix_fields_from_path(args.field_subset_matrix) - 1
        else:
            nbr_subset_matrix_fields = 0
        assigned_annot_dict['subset_matrix'] = AnnotationEntry(self.fasta_id, self.options, 'subset_matrix',
                                                               nbr_subset_matrix_fields)

        return assigned_annot_dict

    def get_entry(self, key):
        return self.assigned_annotation_dict[key]

    def assign_annotation_list(self, annotation_key, value_list):

        if annotation_key not in self.OPTIONAL_ANNOTATIONS:
            raise Exception('Unvalid key "{}" is not included as possible annotation option: {}'
                            .format(annotation_key, self.OPTIONAL_ANNOTATIONS))

        self.assigned_annotation_dict[annotation_key].assign_value_list(value_list)

    def assign_no_annotation(self, key):

        """Used if present annotations already have been assigned"""

        self.assigned_annotation_dict[key].mark_as_assigned()

    def __str__(self):

        entry_string = self.fasta_id
        for key in self.OPTIONAL_ANNOTATIONS:

            if key not in self.INCLUDED_ANNOTATIONS:
                continue

            if self.assigned_annotation_dict.get(key):
                entry_string += self.out_delimit + self.assigned_annotation_dict[key].get_value_string(self.out_delimit)
            else:
                entry_string += self.out_delimit

        return entry_string


class AnnotationEntry:

    """
    Contains information and logic related to single annotation instance
    Can be used to neatly produce an annotation string which can be concatenated
    with other similar annotation strings
    """

    def __init__(self, fasta_id, options_dict, key, nbr_fields):

        self.fasta_id = fasta_id
        self.key = key
        self.nbr_fields = nbr_fields
        self.is_assigned = False
        self.value_list = []

        self.options = options_dict

    def assign_value_list(self, value_list):

        if self.is_assigned:
            if not self.options['quiet']:
                print('WARNING: The annotation with key: {} has already been assigned for ID {}'
                      .format(self.key, self.fasta_id))
                print('Initial value: {} will be used'.format(','.join(self.value_list)))
                print('New value: {} WILL BE IGNORED'.format(','.join(value_list)))
            return

        self.is_assigned = True
        self.value_list = value_list

    def mark_as_assigned(self):

        """
        Marks the entry as assigned if no value is added
        This is used when a limited number of entries already have been assigned, and this entry wasn't included
        """

        if not self.is_assigned:
            self.is_assigned = True

    def has_value(self):
        return len(self.value_list) > 0

    def get_value_string(self, delim):
        if self.is_assigned:
            string_value_list = [str(entry) for entry in self.value_list]
            return delim.join(string_value_list)
        else:
            return delim * (self.nbr_fields - 1)
