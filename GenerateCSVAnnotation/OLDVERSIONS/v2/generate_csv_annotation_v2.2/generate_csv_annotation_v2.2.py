#!/usr/bin/python3

version = "v2.2"
release_date = "16-03-15"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n" \
               "Read different annotation files and output relevant data in CSV format.\n" \
    .format(author, release_date, version)

import csv_annot_modules.data_import as data_import
import csv_annot_modules.args_parsing as args_parsing
import csv_annot_modules.output_writer as output_writer
import csv_annot_modules.utility_functions as util


def main():

    args = args_parsing.parse_arguments(help_message, version)
    gene_entries = data_import.parse_input_entries(args)

    print('Reading annotations...')
    if args.field_blast:
        header_fields = args.blast_columns_headers.split(',')
        value_fields = args.blast_columns.split(',')

        assert len(header_fields) == len(value_fields), \
            print('Must provide same number of blast headers and field numbers!')

    if args.field_subset_matrix:

        nbr_header_fields = len(args.subset_matrix_headers.split(','))
        nbr_first_line_fields = util.nbr_matrix_fields_from_path(args.field_subset_matrix, delim='\t')

        if nbr_first_line_fields > 1:
            assert nbr_first_line_fields == nbr_header_fields, \
                print('Provided header fields ({}) must match non-ID column count ({})'
                      .format(nbr_header_fields, nbr_first_line_fields))

    if args.field_transcripts:
        data_import.import_transcripts(gene_entries, args.field_transcripts, id_sep=args.id_separator,
                                       second_id_sep=args.additional_id_del)

    if args.field_peptides:
        data_import.import_peptides(gene_entries, args.field_peptides, id_sep=args.id_separator,
                                    second_id_sep=args.additional_id_del)

    if args.field_de_matrix:
        data_import.import_de_matrix(gene_entries, args.field_de_matrix, p_cutoff=0.05)

    if args.field_signalp:
        data_import.import_signalp_annotation(gene_entries, args.field_signalp,
                                              sub_id_del=args.additional_id_del)

    if args.field_tmhmm:
        data_import.import_tmhmm_annotation(gene_entries, args.field_tmhmm,
                                            sub_id_del=args.additional_id_del)

    if args.field_blast:
        data_import.import_blast_annotation(gene_entries, args.field_blast, args.blast_columns,
                                            sub_id_del=args.additional_id_del)

    if args.field_subset_matrix:
        data_import.import_subset_matrix_annotation(gene_entries, args.field_subset_matrix,
                                                    sub_id_del=args.additional_id_del)

    if args.field_blast_linkage:
        data_import.import_blast_linkage(gene_entries, args.field_blast_linkage,
                                         sub_id_del=args.additional_id_del)

    print('Number of gene entries: {}'.format(len(gene_entries)))

    print('Writing table...')
    output_writer.output_table(gene_entries, args.output_path, args)

    if args.output_seq_base_name:

        print('Writing sequences...')

        if args.field_transcripts:
            output_writer.output_fasta_seqs(gene_entries, args.field_transcripts,
                                            args.output_seq_base_name + '.cdna.fa', args.id_separator,
                                            args.output_filter_field, args.additional_id_del)

        if args.field_peptides:
            output_writer.output_fasta_seqs(gene_entries, args.field_peptides,
                                            args.output_seq_base_name + '.pep.fa', args.id_separator,
                                            args.output_filter_field, args.additional_id_del)
    print('All done!')


if __name__ == '__main__':
    main()
