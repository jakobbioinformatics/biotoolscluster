__author__ = 'jakob'

from Bio import SeqIO
from Bio.SeqIO import FastaIO


def output_table(gene_entries, output_path, args, delimit=','):

    """Write matrix containing provided annotation information"""

    with open(output_path, 'w') as out_fh:

        # Print header
        header_string = 'TranscriptID'
        if args.field_de_matrix:
            header_string += '{0}baseMeanExpression{0}log2FoldChange{0}absFoldChange{0}adjustedPVal'.format(delimit)
        if args.field_transcripts:
            header_string += '{0}hasTranscript'.format(delimit)
        if args.field_peptides:
            header_string += '{0}hasPeptide'.format(delimit)
        if args.field_signalp:
            header_string += '{0}predictedSignalPeptide'.format(delimit)
        if args.field_tmhmm:
            header_string += '{0}predictedTransmembranceHelices'.format(delimit)
        if args.field_blast_linkage:
            header_string += '{0}blastLinkage'.format(delimit)
        if args.subset_matrix_headers:
            sm_header_fields = args.subset_matrix_headers.split(',')
            for field in sm_header_fields[1:]:  # Skip required ID column
                header_string += '{}{}'.format(delimit, field)
        if args.field_blast:
            blast_header_fields = args.blast_columns_headers.split(',')
            for field in blast_header_fields:
                header_string += '{}{}'.format(delimit, field)

        print(header_string, file=out_fh)

        # Print content
        for entry_key in sorted(gene_entries.keys()):
            target_entry = gene_entries[entry_key]
            if target_entry.get_entry(args.output_filter_field).is_assigned:
                print(str(target_entry), file=out_fh)

        print('{} rows written to {}'.format(len(gene_entries), output_path))


def output_fasta_seqs(gene_entries, in_fasta, output_path, fasta_id_separator, out_filter_field, extra_separator=None):

    target_gene_ids = set()
    for entry_key in gene_entries:
        target_entry = gene_entries[entry_key]
        if target_entry.get_entry(out_filter_field).has_value():
            target_gene_ids.add(entry_key)

    target_sequences = list()
    for record in SeqIO.parse(in_fasta, 'fasta'):

        if not extra_separator:
            my_id = record.id.split(fasta_id_separator)[0]
        else:
            my_id = record.id.split(fasta_id_separator)[0].split(extra_separator)[0]

        if my_id in target_gene_ids:
            target_sequences.append(record)

    with open(output_path, 'w') as out_fh:
        fasta_writer = FastaIO.FastaWriter(out_fh, wrap=None)
        fasta_writer.write_file(target_sequences)

    if len(target_sequences) == 0:
        print('Zero sequences written - Make sure that you are handling the IDs properly')
    else:
        print('{} sequences written to {}'.format(len(target_sequences), output_path))
