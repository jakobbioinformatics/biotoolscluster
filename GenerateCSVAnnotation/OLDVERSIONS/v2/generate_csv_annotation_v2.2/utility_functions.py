__author__ = 'jakob'


def nbr_matrix_fields_from_path(matrix_path, delim='\t'):

    """
    Reads the first line for target file, and returns the number of
    fields present in that file
    """

    with open(matrix_path) as in_fh:
        first_line = in_fh.readline().rstrip()
        first_line_fields = first_line.split(delim)
        return len(first_line_fields)
