__author__ = 'Jakob Willforss'

from Bio import SeqIO
import csv_annot_modules.entry_objects as entry_objects
import csv_annot_modules.data_import_functions.de_matrix as de_matrix


def parse_input_entries(args):

    """Read target nucleotide fasta - The IDs are what is stored"""

    gene_entries = dict()

    if args.id_file_format == 'fasta':
        for record in SeqIO.parse(args.id_file, 'fasta'):
            my_id = record.id.split(args.id_separator)[0]
            my_entry = entry_objects.GeneEntry(my_id, args)
            gene_entries[my_id] = my_entry

    elif args.id_file_format == 'tsv':
        with open(args.id_file) as in_fh:
            for line in in_fh:
                line = line.rstrip()

                fields = line.split('\t')
                my_id = fields[0].split(args.id_separator)[0]
                my_entry = entry_objects.GeneEntry(my_id, args)
                gene_entries[my_id] = my_entry
    else:
        raise Exception("The provided file format '{}' is unvalid or simply not yet implemented"
                        .format(args.id_file_format))
    return gene_entries


def import_de_matrix_pair(gene_entries, de_matrix_pair_fps, p_cutoff, delimitor=','):

    """Read comma-delimited pair of DE matrices"""

    matrix1_fp, matrix2_fp = de_matrix_pair_fps.split(',')

    # TODO: Is the p_cutoff here the part removing the data?
    # TODO: Should default values be set if no value accessible? ('NA')

    # p_cutoff 1, because we filter later in the merge step
    m1_dict = de_matrix.get_de_matrix_data(matrix1_fp, p_cutoff=1, delimitor=delimitor, get_data_as_dict=True)
    m2_dict = de_matrix.get_de_matrix_data(matrix2_fp, p_cutoff=1, delimitor=delimitor, get_data_as_dict=True)

    merged_de_dict = de_matrix.merge_de_matrix_dicts(m1_dict, m2_dict, p_cutoff)

    for entry_id in merged_de_dict:
        value_list = merged_de_dict[entry_id]
        gene_entries[entry_id].assign_annotation_list('de_matrix_pair', value_list)


def import_de_matrix(gene_entries, de_matrix_fp, p_cutoff, delimitor=','):

    de_entry_dict = de_matrix.get_de_matrix_data(de_matrix_fp, p_cutoff, delimitor)

    for entry_id in de_entry_dict:
        value_list = de_entry_dict[entry_id]
        gene_entries[entry_id].assign_annotation_list('de_matrix', value_list)


def import_transcripts(gene_entries, transcripts_fp, id_sep=' ', second_id_sep=None):

    """Check if IDs have provided transcript"""

    transcript_ids = set()
    for record in SeqIO.parse(transcripts_fp, 'fasta'):
        if not second_id_sep:
            my_id = record.id.split(id_sep)[0]
        else:
            my_id = record.id.split(id_sep)[0].split(second_id_sep)[0]
        transcript_ids.add(my_id)


def import_peptides(gene_entries, peptides_fp, id_sep=' ', second_id_sep=None):

    """Check if IDs have provided peptides"""

    transcript_ids = set()
    for record in SeqIO.parse(peptides_fp, 'fasta'):
        if not second_id_sep:
            my_id = record.id.split(id_sep)[0]
        else:
            my_id = record.id.split(id_sep)[0].split(second_id_sep)[0]
        transcript_ids.add(my_id)


def import_signalp_annotation(gene_entries, signalp_path, sub_id_del=None):

    """Add information about if sequences contain identified signal peptide or not"""

    signal_peptide_ids = set()

    with open(signalp_path) as in_fh:
        for line in in_fh:

            if line.startswith('#'):
                continue

            line = line.rstrip()
            fields = line.split()

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            signal_peptide_ids.add(entry_id)

    for peptide_id in signal_peptide_ids:
        value_list = ['Yes']
        gene_entries[peptide_id].assign_annotation_list('signalp', value_list)


def import_tmhmm_annotation(gene_entries, tmhmm_path, sub_id_del=None):

    """Add information about if and how many transmembrane helices the sequences are predicted to contain"""

    with open(tmhmm_path) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split()

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            predicted_helices = int(fields[4].split('=')[1])  # Format: PredHel=0

            if predicted_helices > 0:
                value_list = [str(predicted_helices)]
                gene_entries[entry_id].assign_annotation_list('tmhmm', value_list)


def import_blast_annotation(gene_entries, blast_path, blast_field_string, sub_id_del=None):

    """Read tab delimited blast output and extract desired information by specifying the blast_field_string"""

    with open(blast_path) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            target_field_indices = [int(field)-1 for field in blast_field_string.split(',')]

            target_values = [fields[target_field] for target_field in target_field_indices]

            gene_entries[entry_id].assign_annotation_list('blast', target_values)


def import_subset_matrix_annotation(gene_entries, subset_matrix_fp, sub_id_del=None, output_del=','):

    """
    Import tab delimited matrix containing subset of gene IDs in column 1
    Can contain further information in other columns, which in that case are all included
    """

    found_output_del = False

    with open(subset_matrix_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            if output_del in line:
                found_output_del = True
                line = line.replace(output_del, '')

            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            provided_values = fields[1:]  # Empty if no value columns present
            gene_entries[entry_id].assign_annotation_list('subset_matrix', provided_values)

    if found_output_del:
        print('Found one or more output delimitors ({}) in subset matrix - They have been removed'.format(output_del))


def import_blast_linkage(gene_entries, blast_linkage_fp, sub_id_del=None):

    """
    Import two-column blast linkage file, with blast-related entries in string in second column
    linked to the target IDs in the first column
    """

    with open(blast_linkage_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            fields = line.split('\t')

            if not sub_id_del:
                entry_id = fields[0]
            else:
                entry_id = fields[0].split(sub_id_del)[0]

            blast_linkage_string = fields[1]

            if entry_id in gene_entries:
                gene_entries[entry_id].assign_annotation_list('blast_linkage', [blast_linkage_string])
