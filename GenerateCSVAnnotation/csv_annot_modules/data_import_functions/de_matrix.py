def get_de_matrix_data(de_matrix_fp, p_cutoff, delimitor, get_data_as_dict=False):

    """Retrieve data from DE matrix, returning dictionary linking IDs to values provided in list or dictionary"""

    with open(de_matrix_fp) as in_fh:

        header_entries = next(in_fh).rstrip().split(',')
        id_field = 0
        header_entries = [entry.replace('"', '') for entry in header_entries]

        base_mean_expression_field = header_entries.index('baseMean')
        fold_change_field = header_entries.index('log2FoldChange')
        adjusted_p_field = header_entries.index('padj')

        de_entry_dict = dict()

        for line in in_fh:
            line = line.rstrip()
            de_matrix_fields = [entry.replace('"', '') for entry in line.split(delimitor)]

            entry_id = de_matrix_fields[id_field]
            base_mean_expression = float(de_matrix_fields[base_mean_expression_field])
            fold_change = float(de_matrix_fields[fold_change_field])
            adjusted_p = float(de_matrix_fields[adjusted_p_field])

            if adjusted_p <= p_cutoff:

                if not get_data_as_dict:
                    value_list = [base_mean_expression, fold_change, abs(fold_change), adjusted_p]
                    de_entry_dict[entry_id] = value_list
                else:
                    value_dict = dict()
                    value_dict['base_mean_expression'] = base_mean_expression
                    value_dict['fold_change'] = fold_change
                    value_dict['abs_fold_change'] = abs(fold_change)
                    value_dict['adjusted_p'] = adjusted_p
                    de_entry_dict[entry_id] = value_dict

        return de_entry_dict


def merge_de_matrix_dicts(de_matrix_dict1, de_matrix_dict2, p_cutoff):

    """Reads two dictionaries containing de matrix data, producing a single dict with information about both"""

    all_ids = set(de_matrix_dict1.keys()).union(de_matrix_dict2.keys())

    paired_de_dict = dict()

    for entry_id in all_ids:

        de_m1_exist = de_matrix_dict1.get(entry_id) is not None
        de_m2_exist = de_matrix_dict2.get(entry_id) is not None

        # TODO: Is this removing entry if not passing the p_cutoff?

        if de_m1_exist:
            de_in_m1 = de_matrix_dict1[entry_id]['adjusted_p'] < p_cutoff
        else:
            de_in_m1 = None

        if de_m2_exist:
            de_in_m2 = de_matrix_dict2[entry_id]['adjusted_p'] < p_cutoff
        else:
            de_in_m2 = None

        if de_in_m1 or de_in_m2:

            if de_m1_exist:
                m1_value_dict = de_matrix_dict1[entry_id]

                if de_in_m1:
                    m1_de_present = 'X'
                else:
                    m1_de_present = ''

                m1_base_mean   = m1_value_dict['base_mean_expression']
                m1_fold_change = m1_value_dict['fold_change']
                m1_adjusted_p  = m1_value_dict['adjusted_p']
            else:
                m1_de_present = ''
                m1_base_mean = 'NA'
                m1_fold_change = 'NA'
                m1_adjusted_p = 'NA'

            if de_m2_exist:
                m2_value_dict = de_matrix_dict2[entry_id]

                if de_in_m2:
                    m2_de_present = 'X'
                else:
                    m2_de_present = ''

                m2_base_mean   = m2_value_dict['base_mean_expression']
                m2_fold_change = m2_value_dict['fold_change']
                m2_adjusted_p  = m2_value_dict['adjusted_p']
            else:
                m2_de_present = ''
                m2_base_mean = 'NA'
                m2_fold_change = 'NA'
                m2_adjusted_p = 'NA'

            value_list = [m1_de_present, m2_de_present,
                          m1_base_mean, m1_fold_change, m1_adjusted_p,
                          m2_base_mean, m2_fold_change, m2_adjusted_p]

            paired_de_dict[entry_id] = value_list

    return paired_de_dict
