#!/usr/bin/env python3


import argparse
import pandas as pd

version = "v1.0"
release_date = "17-01-06"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Parse expression matrix to presence/absence matrix" \
    .format(author, release_date, version)

pd.set_option('display.width', 1000)


def main():

    args = parse_arguments()

    df = pd.read_csv(args.count_matrix, index_col=0)

    pa_df = pd.DataFrame(index=df.index)

    headers = args.header_string.split(args.delim)

    total_samples = int(len(df.columns) / args.replicates)
    for i in range(total_samples):
        start_i = i * args.replicates
        end_i = start_i + args.replicates

        print("start: {} end: {}".format(start_i, end_i))

        column_mean_series = df.iloc[:, start_i:end_i].mean(axis=1)
        pa_df[headers[i]] = column_mean_series

    print(pa_df.head())

    output_df = pa_df.applymap(lambda x: x if x >= args.threshold else '')

    output_df.to_csv(args.output_matrix)


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--count_matrix', help='Replicated count matrix, with gene rows and sample columns', required=True)
    parser.add_argument('--replicates', help='Number of replicates per sample. Assumes all to have the same.', type=int, required=True)
    parser.add_argument('--output_matrix', help='Path for output presence/absence matrix')

    parser.add_argument('--sum_type', choices=['mean', 'median', 'sum'], default='mean')
    parser.add_argument('--threshold', help='Cutoff value for being marked as not present', required=True, type=int)

    parser.add_argument('--header_string', help='Comma delimited string with headers for samples')
    parser.add_argument('--replace_gene_names_with_number', action='store_true')

    parser.add_argument('--delim', default=',')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
