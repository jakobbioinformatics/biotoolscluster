#!/bin/bash

version="v1.1"
release_date="15-12-15"
author="Jakob Willforss (Contact: jakob.willforss@hotmail.com)"
help_message="This script takes two genome fasta files and combines them into one.\nIt requires you to provide uniqe tags for each genome which later on can be used to separate them.\nIt also checks both the tags, and that the merged output genome corresponds to the\ncombined provided input genomes."

usage() {
    echo "Usage: $0 -f first_input -F first_unique_tag -s second_input -S second_unique_tag -m merged_output"
}

help() {
    echo
    echo "Genome Merger $version"
    echo "Release date: $release_date"
    echo "Author: $author"
    echo
    echo -e $help_message
    echo
    usage
    echo
    exit 0
}

if [[ $# -eq 0 ]]; then
    help
fi

while getopts ":hhelpf:s:m:F:S:" opt; do
    case $opt in
        f)
            first_input_genome=$OPTARG
            ;;
        F)
            first_unique_tag=$OPTARG
            ;;
        s)
            second_input_genome=$OPTARG
            ;;
        S)
            second_unique_tag=$OPTARG
            ;;
        m)
            merged_output_genome=$OPTARG
            ;;
        h|help)
            help
            ;;
    esac
done

if [[ -z $first_input_genome ]] || [[ -z $second_input_genome ]] || [[ -z $merged_output_genome ]]; then
    echo "Not all required genomes are provided!"
    help
fi

if [[ -z $first_unique_tag ]] || [[ -z $second_unique_tag ]]; then
    echo "You must provide unique tags present in the start of the IDs for both genomes!"
    help
fi

error_found=false

first_id_count=$(grep -c "^>" $first_input_genome)
second_id_count=$(grep -c "^>" $second_input_genome)

first_id_first_tag_count=$(grep -c "^>"$first_unique_tag $first_input_genome)
first_id_second_tag_count=$(grep -c "^>"$second_unique_tag $first_input_genome)
second_id_first_tag_count=$(grep -c "^>"$first_unique_tag $second_input_genome)
second_id_second_tag_count=$(grep -c "^>"$second_unique_tag $second_input_genome)

if (( first_id_second_tag_count != 0)); then
    echo "First tag matched $first_id_second_tag_count sequences, it must match none!"
    error_found=true
fi;

if (( second_id_first_tag_count != 0)); then
    echo "Second tag matched $second_id_first_tag_count sequences, it must match none!"
    error_found=true
fi;

if (( first_id_first_tag_count != first_id_count)); then
    echo "First tag matched $first_id_first_tag_count but should match all $first_id_count sequences!"
    error_found=true
fi

if (( second_id_second_tag_count != second_id_count)); then
    echo "Second tag matched $second_id_second_tag_count but should match all $second_id_count sequences!"
    error_found=true
fi

echo "$error_found"

if [[ $error_found = true ]]; then
    echo "One or more errors encountered. Aborting. Please check your input data."
    exit 1
fi

echo "Tags are ok, proceeding with genome merge. Writing to: $merged_output_genome"

cat $first_input_genome $second_input_genome > $merged_output_genome
chmod -w $merged_output_genome

echo "Verifying merged number of entries and nucleotides"

merged_id_count=$(cat $merged_output_genome | grep -c "^>")
separate_id_count=$(cat $first_input_genome $second_input_genome | grep -c "^>")

if (( merged_id_count != separate_id_count )); then
    echo "Merge FAILED. The number of IDs differ between merged and separate genomes"
    echo "Merged: $merged_id_count ids Separate: $separate_id_count nts"
    mv $merged_output_genome $merged_output_genome.failed
    exit 1
fi

merged_nt_count=$(cat $merged_output_genome | grep -v "^>" | tr -d "\n" | wc -c)
separate_nt_count=$(cat $first_input_genome $second_input_genome | grep -v "^>" | tr -d "\n" | wc -c)

if (( merged_nt_count != separate_nt_count )); then
    echo "Merge FAILED. The number of nucleotides differ between merged and separate genomes"
    echo "Merged: $merged_nt_count nts Separate: $separate_nt_count nts"
    mv $merged_output_genome $merged_output_genome.failed
    exit 1
fi

echo "The merged genome contains same number of ids and nucleotides as the separate genomes"
echo "The merge succeeded, and is found in $merged_output_genome"

echo "Statistics:"
echo "Fasta entries: $merged_id_count"
echo "Nucleotides: $merged_nt_count"


