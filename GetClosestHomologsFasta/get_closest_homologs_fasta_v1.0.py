#!/usr/bin/env python3

import argparse
from Bio import SeqIO

import modules_get_closest_homologs_fasta.query_entry as query_entry
import modules_get_closest_homologs_fasta.blast_entry as blast_entry

version = "v1.0"
release_date = "16-06-14"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Get FASTA with closest homologs from BLAST search(es)." \
    .format(author, release_date, version)

STR_DEL = ','


def main():

    args = parse_arguments()

    verify_input(args)

    labels = args.labels.split(STR_DEL)
    query_entries = parse_queries_to_object_list(args.query_fasta)
    blast_result_files = args.blast_results.split(STR_DEL)
    target_fastas = args.blast_targets.split(STR_DEL)

    for i in range(len(labels)):

        blast_result_file = blast_result_files[i]
        blast_entry_collection = parse_blast_results(blast_result_file, filter_thres=args.blast_cutoff)
        label = labels[i]
        target_fasta = target_fastas[i]

        for entry in query_entries:
            entry.add_blast_results(label, blast_entry_collection)
            entry.assign_fastas_to_best_blast_hits(label, target_fasta, max_hits=args.count_cutoff)

    output_results(query_entries, args.output_dir)


def verify_input(args):

    """Make checks on input to verify that the correct number of entries are present"""

    nbr_blast_paths = len(args.blast_results.split(STR_DEL))
    nbr_target_paths = len(args.blast_targets.split(STR_DEL))
    nbr_labels = len(args.labels.split(STR_DEL))

    assert nbr_target_paths == nbr_blast_paths == nbr_labels, \
        print('Input numbers differs! BLAST res: {}, Targets: {} Labels: {}'
              .format(nbr_blast_paths, nbr_target_paths, nbr_labels))


def parse_queries_to_object_list(query_fp):

    """Parse query sequences and return as a list of QueryEntry-objects"""

    fasta_list = list()
    fasta_iterator = SeqIO.parse(open(query_fp), 'fasta')
    for fs in fasta_iterator:
        query_object = query_entry.QueryEntry(fs.id.split(' ')[0], fs)
        fasta_list.append(query_object)

    return fasta_list


def parse_blast_results(blast_result_fp, filter_thres=1):

    """Takes a comma delimited list of BLAST results and parses them into a BlastEntryCollection object"""

    blast_entry_collection = blast_entry.BlastEntryCollection(filter_thres=filter_thres)

    with open(blast_result_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            blast_entry_collection.add_blast_line(line)

    return blast_entry_collection


def output_results(query_entries, output_dir):

    """Write one fasta file per source"""

    for entry in query_entries:

        result_str_list = entry.get_blast_results_list_fasta_string()
        with open('{}/{}.fs'.format(output_dir, entry.get_query_description()), 'w') as out_fh:
            print('\n'.join(result_str_list), file=out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--query_fasta', help='FASTA file that were queried to get BLAST results', required=True)
    parser.add_argument('--blast_results', help='Comma-separated BLAST results (outfmt 6)', required=True)
    parser.add_argument('--blast_targets', help='FASTA files searched for homologs', required=True)

    parser.add_argument('--output_dir', help='Output directory for generated files', required=True)

    parser.add_argument('--one_fasta_per_query', help='Splits the output FASTA into one per query', action='store_true')
    parser.add_argument('--labels', required=True,
                        help='Comma-delimited labels used in FASTA to distinguish targets (instead of file names)')

    parser.add_argument('--blast_cutoff', type=float, default=1e-6)
    parser.add_argument('--count_cutoff', type=int, default=10)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
