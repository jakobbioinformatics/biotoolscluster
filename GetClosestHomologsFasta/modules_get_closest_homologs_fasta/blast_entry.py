class BlastEntry:

    BLAST_FIELDS = ['query', 'target', 'percent_identity', 'alignment_length', 'mismatches', 'gap_opens',
                    'query_start', 'query_end', 'target_start', 'target_end', 'evalue', 'bit_score']

    def __init__(self, blast_result_line):

        self.blast_result_fields = blast_result_line.split('\t')

        self.query = self._get_field_matching_string('query')
        self.target = self._get_field_matching_string('target')
        self.e_value = float(self._get_field_matching_string('evalue'))

        self.fs_hit_entry = None

    def assign_target_match_fs(self, fs_entry):

        """Assign BioPython FASTA entry representing target match"""

        self.fs_hit_entry = fs_entry

    def get_fs_entry(self):

        """Return added FASTA entry, and throw error if not assigned"""

        if self.fs_hit_entry is None:
            raise Exception('No sequence is assigned!')

        return self.fs_hit_entry

    def get_query(self):
        return self.query

    def get_target(self):
        return self.target

    def passes_e_thres(self, e_thres):

        """Verify that the hit is of a certain level"""

        return self.e_value <= e_thres

    def get_e_value(self):
        return self.e_value

    def _get_field_matching_string(self, match_string):

        """Extract field value using string based on field labels"""

        str_index = self.BLAST_FIELDS.index(match_string)
        return self.blast_result_fields[str_index]


class BlastEntryCollection:

    def __init__(self, filter_thres=1):

        self.blast_entries = list()
        self.filter_thres = filter_thres

        self.matching_fasta_entries = None

    def add_blast_line(self, line):

        """Create BLAST entry from outfmt6 line"""

        blast_entry = BlastEntry(line)
        self.add_blast_entry(blast_entry)

    def add_blast_entry(self, blast_entry):

        """Add BlastEntry object"""

        if blast_entry.passes_e_thres(self.filter_thres):
            self.blast_entries.append(blast_entry)

    def get_collection_for_query(self, target_query):

        """Generate subset of collection related to particular query"""

        query_blast_collection = BlastEntryCollection()

        query_hits = self._get_query_hits(target_query)
        for query_hit in query_hits:
            query_blast_collection.add_blast_entry(query_hit)
        return query_blast_collection

    def get_best_blast_hits(self, target_query, hit_count):

        """Retrieve list containing a certain number of top hits"""

        label_query_hits = self._get_query_hits(target_query)
        sorted_queries = sorted(label_query_hits, key=lambda query: query.get_e_value())
        return sorted_queries[0:hit_count]

    def _get_query_hits(self, target_query):

        """Retrieve all hits matching a certain query"""

        return [hit for hit in self.blast_entries if hit.get_query() == target_query]
