from Bio import SeqIO


class QueryEntry:

    def __init__(self, query_string, query_biopy_entry):

        self.query_string = query_string
        self.query_biopy_entry = query_biopy_entry
        self.blast_results = dict()
        self.best_blast_hits = dict()

    def get_query_description(self):
        return self.query_string

    def add_blast_results(self, label, blast_results_entry_collection):

        """Assign query-specific BLAST results, separated on label"""

        self.blast_results[label] = blast_results_entry_collection

    def has_blast_results(self, label):
        return self.blast_results.get(label) is not None

    def assign_fastas_to_best_blast_hits(self, label, target_sequence_file, max_hits=20):

        """Assign sequences to the best set of BLAST hits"""

        best_blast_results = self.blast_results[label].get_best_blast_hits(self.query_string, max_hits)
        self._assign_fastas_to_blast_results(target_sequence_file, best_blast_results)
        self.best_blast_hits[label] = best_blast_results

    def _assign_fastas_to_blast_results(self, seq_file, blast_hits):

        """Assign sequences to a set of blast hits"""

        target_ids = [hit.get_target() for hit in blast_hits]
        print('Reading from {} for query {}'.format(seq_file, self.query_string))

        fasta_iterator = SeqIO.parse(open(seq_file), 'fasta')
        for fs in fasta_iterator:
            if fs.id in target_ids:

                for hit in blast_hits:
                    if hit.get_target() == fs.id:
                        hit.assign_target_match_fs(fs)

    def get_blast_results_list_fasta_string(self):

        """
        Get string with sequences for best BLAST hits
        Ready to be printed to file
        """

        result_str_list = list()
        result_str_list.append(self._get_output_line(self.query_biopy_entry, 'original_gene'))

        for label in self.best_blast_hits:
            target_blast_hits = self.best_blast_hits[label]
            for target_blast in target_blast_hits:

                result_str_list.append(self._get_output_line(target_blast.get_fs_entry(), label, target_blast))

        return result_str_list

    @staticmethod
    def _get_output_line(fs_entry, source, blast_entry=None):

        if not blast_entry:
            return '>{fs_id},source:{source}\n{seq}'.format(fs_id=fs_entry.id, source=source, seq=fs_entry.seq)
        else:
            return '>{fs_id},source:{source},e-val:{eval}\n{seq}'\
                .format(fs_id=fs_entry.id, source=source, eval=blast_entry.get_e_value(), seq=fs_entry.seq)
