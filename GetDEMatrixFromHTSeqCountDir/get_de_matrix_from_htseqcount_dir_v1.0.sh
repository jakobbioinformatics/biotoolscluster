#!/bin/bash

version="v1.0"
author="Jakob Willforss (Contact: jakob.willforss@hotmail.com"
help_message="This script reads a directory containing HTSeq-count output files, parses them in a temporary dir and outputs a CSV-delimited matrix ready for import to R"

usage() {
    echo "Usage: $0 -d input_dir -o output_matrix"
}

help() {
    echo
    echo "get_de_matrix_from_htseqcount_dir $version"
    echo 
    echo -e "$help_message"
    echo
    usage
    echo
    exit 0
}

if [[ $# -eq 0 ]]; then
    help
fi

while getopts ":hhelpd:o:" opt; do
    case $opt in
        d)
            input_dir=$OPTARG
            ;;
        o)
            output=$OPTARG
            ;;
        h|help)
            help
            ;;
    esac
done

if [[ -z $input_dir || -z $output ]]; then
    echo "You must provide an input directory (-d) and an output path (-o)"
    help
fi

######## Analysis ####################################

echo ""
echo "Input directory: $input_dir"
echo "Output path: $output"

tmp_dir="$(dirname $0)"/get_DE_matrix_tmp
echo "Temp. dir path: $tmp_dir"
mkdir $tmp_dir

echo "Processing samples..."

for sample in "$input_dir"/*; do
    grep -v "^_" $sample | cut -f2 > "$tmp_dir"/onlycount_"${sample##*/}"
done

labels="$tmp_dir"/sample_labels.txt
grep -v "^_" $sample | cut -f1 > $labels

echo "gene_ids,$(ls $input_dir | sed "s/\..*$//" | tr "\n" "," | sed "s/,$//")" > $output
paste -d"," "$labels" "$tmp_dir"/onlycount_* >> $output

rm -r $tmp_dir

echo -e 'Processing done!\n'


