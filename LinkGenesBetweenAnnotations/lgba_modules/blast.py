__author__ = 'jakob'

import lgba_modules.utils as utils

BLAST_COMMAND = '{blast_type} ' \
                '-query {query} ' \
                '-db {db} ' \
                '-evalue {evalue} ' \
                '-num_threads {threads} ' \
                '-max_target_seqs {max_targets} ' \
                '-outfmt 6'


def run_blast(blast_type, query, blast_db, evalue, threads, max_targets, output_path):

    blast_command_string = BLAST_COMMAND.format(blast_type=blast_type,
                                                query=query,
                                                db=blast_db,
                                                evalue=evalue,
                                                threads=threads,
                                                max_targets=max_targets)

    full_output_path = output_path + '/outblast.outfmt6'

    utils.run_subshell(blast_command_string, output_path=full_output_path)
