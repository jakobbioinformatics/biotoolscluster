__author__ = 'jakob'

import lgba_modules.utils as utils


def parse_blast_to_id_dicts(blast_fp, output_base, query_subset_fp=None, subject_subset_fp=None):

    """Parse BLAST tsv-file into two linkage tables for query and subject"""

    print('Retrieving BLAST entries')
    blast_entries = _get_blast_entries(blast_fp)

    print('Generating linkage dicts')
    queries_linkages_dict = _get_linkage_dict(blast_entries,
                                              target_category='query',
                                              target_subset_fp=query_subset_fp,
                                              linkage_subset_fp=subject_subset_fp)

    subjects_linkages_dict = _get_linkage_dict(blast_entries,
                                               target_category='subject',
                                               target_subset_fp=subject_subset_fp,
                                               linkage_subset_fp=query_subset_fp)

    print('Writing linkage dicts to file')
    output_linkage_to_file(queries_linkages_dict, output_base + 'query_linkage.tsv')
    output_linkage_to_file(subjects_linkages_dict, output_base + 'subjects_linkage.tsv')

    print('All done!')


def _get_linkage_dict(blast_entries, target_category='query', target_subset_fp=None, linkage_subset_fp=None):

    """Retrieve dictionary linking target category to IDs of linkage category"""

    linkages_dict = dict()

    target_subset_dict = list()
    if target_subset_fp:
        target_subset_dict = utils.read_dict_from_column_file(target_subset_fp)

    linkage_subset_dict = list()
    if linkage_subset_fp:
        linkage_subset_dict = utils.read_dict_from_column_file(linkage_subset_fp)

    for entry in blast_entries:
        if target_category == 'query':
            target = entry.query_id
            linkage = entry.subject_id
        elif target_category == 'subject':
            target = entry.subject_id
            linkage = entry.query_id
        else:
            raise Exception('Unvalid target: {}'.format(target_category))

        if target_subset_fp:
            if target not in target_subset_dict:
                continue

        annot_str = ''
        if linkage_subset_fp:
            if linkage not in linkage_subset_dict:
                continue
            else:
                annot_str = linkage_subset_dict[linkage]

        evalue = entry.evalue
        score = entry.score
        linkage_string = '{link}({evalue}|{score}{annot});'.format(link=linkage, evalue=evalue,
                                                                   score=score, annot=annot_str)

        if not linkages_dict.get(target):
            linkages_dict[target] = linkage_string
        else:
            linkages_dict[target] += linkage_string

    return linkages_dict


def _get_blast_entries(blast_fp):

    """Parse a BLAST file (outfmt6) and return a list of BLAST entries"""

    blast_entries = list()

    with open(blast_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            blast_entry = BlastEntry(line)
            blast_entries.append(blast_entry)

    return blast_entries


def output_linkage_to_file(linkage_dict, out_fp):

    """Outputs target linkage dict to two-column tsv file"""

    with open(out_fp, 'w') as out_fh:
        for target in sorted(linkage_dict):
            print('{}\t{}'.format(target, linkage_dict[target]), file=out_fh)


class BlastEntry:

    """Encapsulates information from single BLAST line"""

    def __init__(self, blast_string):

        blast_fields = blast_string.split('\t')
        self.query_id = blast_fields[0]
        self.subject_id = blast_fields[1]
        self.score = blast_fields[9]
        self.evalue = blast_fields[10]
