__author__ = 'jakob'

import subprocess

def run_subshell(command, output_path=None):

    """Executes a shell command using the subprocess module"""

    command_list = command.split(' ')

    print('Executing command: {}'.format(command))

    if output_path:
        with open(output_path, 'w') as out_fh:
            subprocess.call(command_list, stdout=out_fh)
    else:
        subprocess.call(command_list)

    print('Command finished!')


def read_list_from_column_file(column_fp):

    """
    Reads a single-column file and parses it into a list
    Parses out first column if tab delimited file supplied
    """

    id_list = list()
    with open(column_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            id_list.append(line.split('\t')[0])

    return id_list


def read_dict_from_column_file(column_fp):

    """
    Reads a multi-column file and parses it into a dict
    The first column is put as key, the rest is merged to one |-delimited string
    """

    column_dict = dict()
    with open(column_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()
            line_fields = line.split('\t')
            id_string = line_fields[0]

            if len(line_fields) > 1:
                annot_string = '|' + '|'.join(line_fields[1:])
            else:
                annot_string = ''

            column_dict[id_string] = annot_string

    return column_dict
