#!/usr/bin/python3

version = "v1.1"
release_date = "16-03-16"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Generate table with annotation genes linked to related transcript IDs." \
               "" \
               "Contains two submodules: blast and link" \
               "" \
               "The link module takes a blast output file and links related IDs"\
    .format(author, release_date, version)

import argparse

import lgba_modules.blast as blast
import lgba_modules.link as link


def blast_main(args):

    evalue = 1
    max_targets = 10000

    blast.run_blast(blast_type=args.blast_mode,
                    query=args.in_transcripts,
                    blast_db=args.blast_db,
                    evalue=evalue,
                    threads=args.threads,
                    max_targets=max_targets,
                    output_path=args.output)


def link_main(args):
    link.parse_blast_to_id_dicts(args.blast_out_fmt6,
                                 args.output_base,
                                 query_subset_fp=args.query_subset_matrix,
                                 subject_subset_fp=args.subject_subset_matrix)


def parse_arguments():

    def default_func(args):
        print("Must specify a sub-parser, exitting...")
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: blast link')

    blast_parser = subparsers.add_parser('blast')
    blast_parser.set_defaults(func=blast_main)
    blast_parser.add_argument('--in_transcripts', help='Input transcripts', required=True)
    blast_parser.add_argument('--blast_db', help='Target BLAST database', required=True)
    blast_parser.add_argument('--threads', help='Number of threads for blasting', default=1)
    blast_parser.add_argument('--blast_mode', help='What type of BLAST to run', default='blastx',
                              choices=['blastx', 'blastp', 'blastn'])
    blast_parser.add_argument('--output', help='Blast output file', required=True)

    link_parser = subparsers.add_parser('link')
    link_parser.set_defaults(func=link_main)
    link_parser.add_argument('--blast_out_fmt6', help='Tab-delimited blast output', required=True)
    link_parser.add_argument('--query_subset_matrix',
                             help='1-2 column list with desired query targets and optional annotation column')
    link_parser.add_argument('--subject_subset_matrix',
                             help='1-2 column list with desired subject targets and optional annotation column')
    link_parser.add_argument('--output_base', help='Base name for output link files')

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_arguments()
