#!/usr/bin/python3

import argparse
from Bio import SeqIO

version = "v1.0"
release_date = "16-06-27"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Generate FASTA and taxa file used for custom QIIME annotation database" \
    .format(author, release_date, version)


def main():

    args = parse_arguments()

    fasta_dict = read_fasta_files_to_dict(args.input_annotated_fasta)
    output_qiime_fasta(fasta_dict, args.output_qiime_fasta)

    if args.base_taxa == 'phytophthora':
        base_taxa = 'Eukaryota;Heterokontophyta;Oomycetes;Peronosporales;Pythiaceae;Phytophthora'
    else:
        raise ValueError('The provided base taxa is unknown: {}'.format(args.base_taxa))

    output_qiime_taxa(fasta_dict, args.output_qiime_taxa, base_taxa)


def read_fasta_files_to_dict(input_fp, id_separator=' '):

    """Parse FASTA file into BioPython entries"""

    fasta_entries = dict()

    for record in SeqIO.parse(input_fp, 'fasta'):
        my_id = record.id.split(id_separator)[0]
        fasta_entries[my_id] = record

    return fasta_entries


def output_qiime_fasta(fasta_dict, out_fp):

    """Output FASTA entries using key as ID"""

    with open(out_fp, 'w') as out_fh:
        for key in fasta_dict:
            my_seq = fasta_dict[key].seq
            print('>{ID}\n{seq}'.format(ID=key, seq=my_seq), file=out_fh)


def output_qiime_taxa(fasta_dict, out_fp, base_taxa):

    """Output taxonomy for each FASTA ID"""

    with open(out_fp, 'w') as out_fh:

        for key in fasta_dict:

            # Retrieve all but first space delimited field
            species_taxa = ' '.join(fasta_dict[key].description.split(' ')[1:])
            taxon_string = get_taxonomy_string(base_taxa, species_taxa)

            print('{ID}\t{taxon_string}'.format(ID=key, taxon_string=taxon_string), file=out_fh)


def get_taxonomy_string(base_taxa_string, species_taxa, base_taxa_del=';'):

    """Generate QIIME formatted taxonomy string"""

    # Base taxa: kingdom; phylum; class; order; family; genus (no species)

    string_prefixes = ['k__', 'p__', 'c__', 'o__', 'f__', 'g__', 's__']
    taxa_fields = base_taxa_string.split(base_taxa_del)
    taxa_fields.append(species_taxa)

    final_taxa_fields = [string_prefixes[i] + taxa_fields[i] for i in range(len(taxa_fields))]

    return base_taxa_del.join(final_taxa_fields)


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--input_annotated_fasta', required=True)
    parser.add_argument('--base_taxa', choices=['phytophthora'], required=True)
    parser.add_argument('--output_qiime_fasta', required=True)
    parser.add_argument('--output_qiime_taxa', required=True)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
