#!/usr/bin/python3

version = "v1.3"
release_date = "15-12-17"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\nThe purpose of this script is to collapse isoform transcript in Cuffmerge output GTF-files.".format(author, release_date, version)

import argparse
import sys


def main():

    args = parse_arguments()
    gtf_collections = get_gtf_collections(args.in_gtf)

    if args.debug:
        print_debug_output(gtf_collections)
        exit(0)

    if args.out_merged_gtf:
        with open(args.out_merged_gtf, 'w') as out_fh:
            output_merged_collections(gtf_collections, out_fh)
    else:
        output_merged_collections(gtf_collections, sys.stdout)


def output_merged_collections(gtf_collections, out_fh):

    """Output merged GTF entries to provided output handle"""

    for key in sorted(gtf_collections):
        collection = gtf_collections[key]
        merged_entries = collection.get_merged_entries()

        for entry in merged_entries:
            print(entry, file=out_fh)


def print_debug_output(gtf_collections):

    """Outputs debug information"""

    for key in gtf_collections:
        collection = gtf_collections[key]
        merged_entries = collection.get_merged_entries()

        print("-------- Unmerged --------")
        for entry in collection.gtf_entries:
            print(entry)

        print("--------  Merged  --------")
        for entry in merged_entries:
            print(entry)


def get_gtf_collections(gtf_fp):

    """
    Read and parse GTF file into collections where entries with the same sequence ID
    are grouped together
    """

    gtf_collections = dict()

    with open(gtf_fp, 'r') as gtf_fh:
        for gtf_line in gtf_fh:

            gtf_line = gtf_line.rstrip()
            gtf_entry = GTFEntry(gtf_line)

            gtf_collection_key = gtf_entry.attributes['gene_id']
            if gtf_collections.get(gtf_collection_key):
                collection = gtf_collections[gtf_collection_key]
                collection.add_gtf_entry(gtf_entry)
            else:
                collection = GTFCollection(gtf_collection_key)
                collection.add_gtf_entry(gtf_entry)
                gtf_collections[gtf_collection_key] = collection

    return gtf_collections

def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--in_gtf', help='Input gtf file', required=True)
    parser.add_argument('-o', '--out_merged_gtf', help='Output merged GTF file')
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    return args


class GTFCollection:

    def __init__(self, collection_key):

        self.collection_key = collection_key
        self.gtf_entries = []

        self.gene_id = None
        self.transcript_id = None

    def add_gtf_entry(self, gff_entry):

        """Adds a GTF file to the collection of GTF files"""

        self.gtf_entries.append(gff_entry)
        self.gtf_entries.sort(key=lambda entry: entry.start)

        if self.gene_id is None:
            assert self.transcript_id is None, print("If collection's gene_id is uninitialized, so should the transcript_id be!")
            self.gene_id = self.gtf_entries[0].attributes['gene_id']
            self.transcript_id = self.gtf_entries[0].attributes['transcript_id']

    def get_merged_entries(self):

        """
        Returns a list of GTF entries
        Overlapping entries are merged together, while non-overlapping are returned as they are
        Adjusted for Cufflinks output, retaining only the gene_id in the description string
        """

        merged_entries = []
        current_entry = None
        for i in range(len(self.gtf_entries)):

            if current_entry is None:
                current_entry = self.gtf_entries[i]

            if i + 1 < len(self.gtf_entries):
                next_entry = self.gtf_entries[i+1]

                if not current_entry.stretches_forward_into(next_entry):
                    merged_entries.append(current_entry.get_merged_entry(self.gene_id, self.transcript_id, other_entry=None))
                    current_entry = None
                else:
                    current_entry = current_entry.get_merged_entry(self.gene_id, self.transcript_id, other_entry=next_entry)

        if current_entry:
            merged_entries.append(current_entry.get_merged_entry(self.gene_id, self.transcript_id, other_entry=None))

        return merged_entries

class GTFEntry:

    """
    Represents a single GTF entry
    Names of columns taken from http://www.ensembl.org/info/website/upload/gff.html
    """

    def __init__(self, gtf_line):
        self.gtf_line = gtf_line
        gtf_entries = gtf_line.split('\t')

        self.seqname = gtf_entries[0]
        self.source = gtf_entries[1]
        self.feature = gtf_entries[2]
        self.start = int(gtf_entries[3])
        self.end = int(gtf_entries[4])
        self.score = gtf_entries[5]
        self.strand = gtf_entries[6]
        self.frame = gtf_entries[7]

        self.attributes_string = gtf_entries[8]
        self.attributes = self._get_attributes_dict(self.attributes_string)

    def stretches_forward_into(self, other_entry):

        """Returns True or False based on whether this entry stetches into the proceeding entry"""

        return self.end > other_entry.start

    def get_merged_entry(self, gene_id, transcript_id, other_entry=None):

        """Returns a new entry merging this entry with a proceeding"""

        if other_entry is not None:
            new_end = max(self.end, other_entry.end)
            assert self.strand == other_entry.strand, \
                print("Need to take strandedness into account!! Strands: {} and {}".format(self.strand, other_entry.strand))
        else:
            new_end = self.end

        attribute_string = '{} "{}"; {} "{}";'\
            .format('gene_id', gene_id, 'transcript_id', transcript_id)
        new_gtf_line = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(self.seqname, self.source, self.feature, self.start,
                                                                   new_end, self.score, self.strand, self.frame,
                                                                   attribute_string)
        return GTFEntry(new_gtf_line)

    @staticmethod
    def _get_attributes_dict(attributes_string, splitter=';', attr_splitter=' ', val_trim='"'):

        """
        Takes a string with attributes, and returns a dictionary with key-value pairs
        The user is able to provide splitting characters and optionally a trimming character
        """

        attributes = [attr.strip() for attr in attributes_string.split(splitter)]
        attr_dict = dict()
        for attr in attributes[0:-1]:
            key, value = [val.strip(val_trim) for val in attr.split(attr_splitter)]
            attr_dict[key] = value
        return attr_dict

    def __str__(self):
        return self.gtf_line

if __name__ == '__main__':
    main()
