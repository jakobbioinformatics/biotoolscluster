#!/usr/bin/python3

version = "v1.1"
release_date = "16-06-08"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\nThe purpose of this script is to merge related fasta sequences identified by a part of their header. It can for example merge exon sequences identified by a common gene-id.".format(author, release_date, version)

import argparse

def main():

    args = parse_arguments()
    fasta_entries = get_fasta_entries(args.in_fasta, args.delimitor, args.field_number)

    with open(args.out_fasta, 'w') as out_fh:
        for entry in sorted(fasta_entries):

            if args.behaviour == 'retain_long':
                output_fs = get_long_fasta(fasta_entries[entry])
            elif args.behaviour == 'concatenate':
                output_fs = get_merged_fasta(fasta_entries[entry])
            else:
                raise Exception('Unknow behaviour option: {}'.format(args.behaviour))

            print(output_fs, file=out_fh)


def get_fasta_entries(fasta_path, delimitor, field_number):

    """Parses single-line formatted fasta into fasta entries"""

    fasta_entries = dict()

    with open(fasta_path, 'r') as in_fh:
        for line in in_fh:
            line = line.rstrip()
            assert line.startswith(">"), print("Non-valid fasta header: {}".format(line))
            header = line.lstrip(">")
            seq = next(in_fh).rstrip()

            fasta_entry = FastaEntry(header, seq)
            header_fields = header.split(delimitor)
            target_field = header_fields[field_number]

            if fasta_entries.get(target_field):
                fasta_entries[target_field].append(fasta_entry)
            else:
                fasta_entries[target_field] = [fasta_entry]

    return fasta_entries


def get_merged_fasta(field_entries):

    tot_header = '|'.join(entry.header for entry in field_entries)
    tot_seq = ''.join(entry.sequence for entry in field_entries)

    return FastaEntry(tot_header, tot_seq)


def get_long_fasta(field_entries):

    longest_seq_entry = field_entries[0]
    for entry in field_entries:
        if len(entry.sequence) > len(longest_seq_entry.sequence):
            longest_seq_entry = entry
    return longest_seq_entry


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', '--in_fasta', help='Provided input fasta', required=True)
    parser.add_argument('-d', '--delimitor', help='Field delimitor for fasta headers', required=True)
    parser.add_argument('-f', '--field_number', help='The field to merge on (zero based)', required=True, type=int)
    parser.add_argument('-o', '--out_fasta', help='Parsed output fasta')
    parser.add_argument('--behaviour', help='How to handle matching entries', choices=['concatenate', 'retain_long'],
                        default='retain_long')
    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()
    return args

class FastaEntry:

    def __init__(self, header, sequence):
        self.header = header
        self.sequence = sequence

    def __str__(self):
        return '>{}\n{}'.format(self.header, self.sequence)

if __name__ == '__main__':
    main()


