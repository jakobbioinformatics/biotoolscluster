#!/usr/bin/env python3

import argparse


def main():

    args = parse_arguments()

    with open(args.in_go_east_format) as in_fh, open(args.out_two_col_format, 'w') as out_fh:
        
        for line in in_fh:
            line = line.rstrip()

            id_field, go_field = line.split('\t')

            go_fields = go_field.split(' // ')

            for go in go_fields:
                print('{}\t{}'.format(id_field, go), file=out_fh)



def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--in_go_east_format')
    parser.add_argument('--out_two_col_format')

    return parser.parse_args()


if __name__ == "__main__":
    main()

