#!/usr/bin/env python3

import argparse


def main():

    args = parse_arguments()

    id_gos_dict = get_id_gos_dict(args.in_two_col)
    write_go_east_format(args.out_go_east, id_gos_dict)
    
    print('All done!')


def get_id_gos_dict(in_two_col_fp):

    """Parse two column input file and store ID/GO in dict"""

    id_gos_dict = dict()

    with open(in_two_col_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            id_field, go_field = line.split('\t')

            if not id_field in id_gos_dict:
                id_gos_dict[id_field] = [go_field]
            else:
                id_gos_dict[id_field].append(go_field)

    return(id_gos_dict)


def write_go_east_format(out_go_east_fp, id_gos_dict):

    """
    Write entries from ID GOs dict to GOEAST format
    
    ID  GO // GO // GO
    """

    prot_ids = 0
    go_terms = 0

    with open(out_go_east_fp, 'w') as out_fh:
        for prot_id in sorted(id_gos_dict):
            print('{}\t{}'.format(prot_id, ' // '.join(id_gos_dict[prot_id])), file=out_fh)
            prot_ids += 1
            go_terms += len(id_gos_dict[prot_id])

    print('{} GO terms assigned to {} protein IDs'.format(go_terms, prot_ids))


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument('--in_two_col', required=True)
    parser.add_argument('--out_go_east', required=True)

    return parser.parse_args()


if __name__ == "__main__":
    main()

