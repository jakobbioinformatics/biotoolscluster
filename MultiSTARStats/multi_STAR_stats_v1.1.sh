#!/bin/bash

version="v1.1"
author="Jakob Willforss (Contact: jakob.willforss@hotmail.com"
help_message="This script quickly overviews a number of STAR output directories located together in a directory. It takes the parent directory as input, and provides a short overview of all the mappings."

usage() {
    echo "Usage: $0 -d parent_directory"
}

help() {
    echo
    echo "Multi-STAR-stats ${version}"
    echo
    echo -e ${help_message}
    echo
    usage
    echo
    exit 0
}

if [[ $# -eq 0 ]]; then
    help
fi

while getopts ":hhelpd:" opt; do
    case $opt in
        d)
            parent_directory=$OPTARG
            ;;
        h|help)
            help
            ;;
    esac
done

if [[ -z ${parent_directory} ]]; then
    echo "You must provide a parent directory"
    help
fi

target_log_file="*Log.final.out"

echo -e "Dir\tMappedReads(%)"
for dir in "${parent_directory}"/*; do

    dir_name=${dir##*/}

    if [[ ! $(\ls -1 ${dir}/${target_log_file} 2>/dev/null) ]]; then
        echo "${dir_name} doesn't seem to be a valid STAR output directory, no ${target_log_file} found"
        continue
    fi

    uniq=$(grep "Uniquely mapped reads %" $dir/${target_log_file} | cut -f2 | sed "s/%$//")
    multi=$(grep "% of reads mapped to multiple loci" ${dir}/${target_log_file} | cut -f2 | sed "s/%$//")
    tot=$(echo "${uniq}+${multi}" | bc)
    echo -e "${dir_name}\t${tot}"
done                                                                          

