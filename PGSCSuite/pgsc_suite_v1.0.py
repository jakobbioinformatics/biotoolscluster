#!/usr/bin/python3

import argparse
import re

version = "v1.0"
release_date = "16-05-23"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Utilities for managing the PGSC genome." \
    .format(author, release_date, version)

ID_FORMATS = ['gene', 'transcript', 'cds', 'peptide']
ID_PATTERNS = [r'PGSC0003DMG', r'PGSC0003DMT', r'PGSC0003DMC', r'PGSC0003DMP']


def translate_ids_main(args):

    id_map_dict = get_id_map_dict(args.pgsc_mapping_matrix, args.input_id_format, args.output_id_format)
    output_parsed_file(args.input, args.output, id_map_dict, args.input_id_format)


def output_parsed_file(input_fp, output_fp, id_map_dict, input_format):

    """Match target input IDs, and replace them according to the id_map_dict, before writing to file"""

    id_index = ID_FORMATS.index(input_format)
    target_id_pattern = ID_PATTERNS[id_index]
    id_regex = target_id_pattern + r'\d+'

    with open(input_fp) as in_fh, open(output_fp, 'w') as out_fh:
        for line in in_fh:
            line = line.rstrip()

            output_line = line

            if target_id_pattern in line:

                matches_list = re.findall(id_regex, line)

                for match in matches_list:
                    output_line = output_line.replace(match, id_map_dict[match])

            print(output_line, file=out_fh)


def get_id_map_dict(mapping_matrix_fp, input_format, output_format):

    """
    Retrieves desired input/output columns from the mapping matrix
    and returns them as a dictionary
    """

    if input_format not in ID_FORMATS:
        raise Exception("Input format: {} not found in valid formats: {}".format(input_format, ID_FORMATS))

    if output_format not in ID_FORMATS:
        raise Exception("Output format: {} not found in valid formats: {}".format(output_format, ID_FORMATS))

    id_map_dict = dict()

    with open(mapping_matrix_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            fields = line.split('\t')
            in_index = ID_FORMATS.index(input_format)
            out_index = ID_FORMATS.index(output_format)

            in_id = fields[in_index]
            out_id = fields[out_index]

            id_map_dict[in_id] = out_id

    return id_map_dict


def parse_arguments():

    def default_func(args):
        print("Must specify a sub-parser, exiting...")
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: translate_ids')

    translate_ids_parser = subparsers.add_parser('translate_ids')
    translate_ids_parser.set_defaults(func=translate_ids_main)
    translate_ids_parser.add_argument('--input', help='Input file to be updated, any format is OK', required=True)
    translate_ids_parser.add_argument('--output', help='The parsed output file', required=True)
    translate_ids_parser.add_argument('--input_id_format', help='Format to parse the IDs from',
                                      required=True, choices=ID_FORMATS)
    translate_ids_parser.add_argument('--output_id_format', help='Format to parse the IDs to',
                                      required=True, choices=ID_FORMATS)
    translate_ids_parser.add_argument('--pgsc_mapping_matrix', help='The PGSC ID mapping matrix', required=True)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_arguments()
