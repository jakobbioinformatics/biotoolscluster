#!/usr/bin/env python3

import argparse
import collections

version = 'v1.0'
release_date = '16-06-09'
author = 'Jakob Willforss (jakob.willforss@hotmail.com)'
description = 'Utilities to prepare custom GO annotation files, in particular for AgriGO'
help_message = 'Author: {}\nRelease date: {}\nVersion: {}\n\n' \
               '{}' \
    .format(author, release_date, version, description)


def prepare_background_main(args):

    ordered_parsed_back_dict = read_background_dict(args.raw_background)
    output_go_dict(args.out_background, ordered_parsed_back_dict,
                   args.one_line_per_loci, args.go_prefix)


def annotate_de_main(args):

    parsed_back_dict = read_background_dict(args.raw_background)
    de_set = get_de_set(args.de_list)

    de_genes_annot_dict = {key: value for key, value in parsed_back_dict.items() if key in de_set}

    output_go_dict(args.out_annot_de_list, de_genes_annot_dict,
                   args.one_line_per_loci, args.go_prefix)


def read_background_dict(raw_back_fp):

    """Load raw background into dictionary"""

    ordered_background_dict = collections.OrderedDict()

    with open(raw_back_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            loci_id, go_string = line.split('\t')
            go_entries = go_string.split(',')

            if go_entries[0] == 'null':
                continue

            if ordered_background_dict.get(loci_id):
                ordered_background_dict[loci_id].union(set(go_entries))
            else:
                ordered_background_dict[loci_id] = set(go_entries)

    return ordered_background_dict


def output_go_dict(output_fp, go_dict, one_line_per_loci=False, go_prefix=None):

    """Write dictionary containing GO terms to file"""

    with open(output_fp, 'w') as out_fh:
        for loci_id in go_dict:

            go_terms = go_dict[loci_id]

            if go_prefix:
                go_terms = [update_prefix(go, go_prefix) for go in go_terms]

            if not one_line_per_loci:
                for go_term in go_terms:
                    print('{}\t{}'.format(loci_id, go_term), file=out_fh)
            else:
                print('{}\t{}'.format(loci_id, ','.join(go_terms)), file=out_fh)


def update_prefix(go_term, new_prefix):

    """Extract and change prefix coming before expected colon sign in the ID"""

    go_term_split = go_term.split(':')
    go_term_split[0] = new_prefix
    updated_go_term = ':'.join(go_term_split)
    return updated_go_term


def get_de_set(de_fp):

    """Parse input DE genes to a list"""

    de_set = set()
    with open(de_fp) as in_fh:
        for de_gene in in_fh:
            de_gene = de_gene.rstrip()
            de_set.add(de_gene)
    return de_set


def parse_arguments():

    def default_func(args):
        print('Must specify a sub-parser, exiting...')
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: prepare_background, annotate_de')

    # Prepare background

    prep_background_parser = subparsers.add_parser('prepare_background',
                                                   help='Parse raw redundant background to non-redundant and cleaned')
    prep_background_parser.set_defaults(func=prepare_background_main)

    prep_background_parser.add_argument('--raw_background',
                                        help='Two-column format with redundant IDs and comma delimited GO terms',
                                        required=True)
    prep_background_parser.add_argument('--out_background',
                                        help='Prepared background',
                                        required=True)
    prep_background_parser.add_argument('--one_line_per_loci',
                                        action='store_true')
    prep_background_parser.add_argument('--go_prefix',
                                        help='Replace variations on GO annot with prefix like "GO"',
                                        default='GO')

    # Annotate DE

    annotate_de_parser = subparsers.add_parser('annotate_de',
                                               help='Prepare annotated gene list using prepare annotation')
    annotate_de_parser.set_defaults(func=annotate_de_main)

    annotate_de_parser.add_argument('--raw_background',
                                    help='Two-column format with redundant IDs and comma delimited GO terms',
                                    required=True)
    annotate_de_parser.add_argument('--de_list',
                                    help='Single-column list of DE genes',
                                    required=True)
    annotate_de_parser.add_argument('--out_annot_de_list',
                                    help='Annotated DE genes (two-column tsv)',
                                    required=True)
    annotate_de_parser.add_argument('--one_line_per_loci',
                                    action='store_true')
    annotate_de_parser.add_argument('--go_prefix',
                                        help='Replace variations on GO annot with prefix like "GO"',
                                        default='GO')

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_arguments()
