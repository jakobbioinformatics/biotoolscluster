#!/usr/bin/python3

import argparse
DESCRIPTION = """This script reformats and adds a sample ID to all FASTQ headers in a FASTQ file

Initial FASTQ file:

@readid
ATGCATGC
+
:://!!#!

Parsed FASTQ file:

@samplename_readnumber readid
ATGCATGC
+
:://!!#!

Author: Jakob Willforss
Date: 160126
Version: 1.0.0
"""


def main():

    args = parse_arguments()

    entry_number = 0
    line_number = 0
    with open(args.input, 'r') as in_fh, open(args.output, 'w') as out_fh:
        for line in in_fh:
            line = line.rstrip()
            line_number += 1

            if (line_number % 4 == 1):
                entry_number += 1
                readid = line[1:]
                new_header = '@{}_{} {}'.format(args.sample_name, entry_number, readid)
                #new_header = '@{}_{} {}'.format(readid, entry_number, args.sample_name)
                print(new_header, file=out_fh)
            else:
                print(line, file=out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser(DESCRIPTION)
    parser.add_argument('-i', '--input', help='Provided input file', required=True)
    parser.add_argument('-o', '--output', help='Target for output file', required=True)
    parser.add_argument('--sample_name', help='The name of the sample', required=True)
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()



