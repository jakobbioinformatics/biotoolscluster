#!/bin/bash

version="v1.3"
release_date='16-03-03'
author="Jakob Willforss (Contact: jakob.willforss@hotmail.com"
help_message="Quickly run a BLAST for a subsample of a read file and retrieve highly abundant taxa.
Can read gzipped input."

usage() {
    echo "Usage: $0 -f sequence_file -b blast_database -o output_directory [-d def:1000] [-t def:1]"
}

help() {
    echo
    echo "quick_taxa_BLAST_check_v1.3.sh $version"
    echo "Released ${release_date}"
    echo
    echo -e $help_message
    echo
    usage
    echo
    exit 0
}

if [[ $# -eq 0 ]]; then
    help
fi

depth=1000
threads=1

while getopts ":hhelp:d:f:b:o:t:" opt; do
    case $opt in
        f)
            fx=$OPTARG
            ;;
        b)
            blast_db=$OPTARG
            ;;
        d)
            depth=$OPTARG
            ;;
        t)
            threads=$OPTARG
            ;;
        o)
            out_dir=$OPTARG
            ;;
        h|help)
            help
            ;;
    esac
done

############################################

all_options_valid=true

if [[ ! ${depth} =~ ^[0-9]+$ ]]; then
    echo "Sample depth must be a positive integer, received: ${depth}"
    all_options_valid=false
fi

if [[ ! ${threads} =~ ^[0-9]+$ ]]; then
    echo "Threads option must be a positive integer, received: ${depth}"
    all_options_valid=false
fi
if [[ -z ${fx} ]]; then
    echo "Must set fx option"
    all_options_valid=false
fi

if [[ -z ${out_dir} ]]; then
    echo "Must set output directory"
    all_options_valid=false
fi

if [[ -z ${blast_db} ]]; then
    echo "Must assign BLAST database"
    all_options_valid=false
fi

if [[ ! ${all_options_valid} == true ]]; then
    echo "One or more options was not valid, aborting..."
    exit 1
fi

#############################################

echo "Checking that required programs are present in PATH..."
seqtk=seqtk
blast=blastx
all_present=true

if [[ ! $(type ${seqtk} 2>/dev/null) ]]; then
    echo "${seqtk} not found!"
    all_present=false
fi

if [[ ! $(type ${blast} 2>/dev/null) ]]; then
    echo "${blast} not found!"
    all_present=false
fi

if [[ ! ${all_present} = true ]]; then
    echo "Not all required programs found in path"
    exit 1
fi

# Settings update
#############################################

echo ""
echo "--- Settings ---"
echo "Input file: ${fx}"
echo "Output directory: ${out_dir}"
echo "BLAST database: ${blast_db}"
echo "Threads: ${threads}"
echo "Sample depth: ${depth}"
echo ""

#############################################

fx_filename=${fx##*/}

fasta_subset=${out_dir}/${fx_filename}.subset
blast_out=${out_dir}/${fx_filename%%.*}.blastx.outfmt6
stats_out=${out_dir}/${fx_filename%%.*}.stats.txt

echo "Extracting sample subset..."

seqtk sample ${fx} ${depth} | seqtk seq -a - > ${fasta_subset}

echo "Running BLAST..."

blastx \
    -query ${fasta_subset} \
    -db ${blast_db} \
    -num_threads ${threads} \
    -max_target_seqs 1 \
    -outfmt 6 > ${blast_out}

echo "Extracting taxas"

cut -f2 ${blast_out} | cut -f2 -d"_" | sort | uniq -c | sort -n | tail -20 | sort -n -r | sed "s/^ *//" | tr " " "\t" > ${stats_out}

echo "All done!"


