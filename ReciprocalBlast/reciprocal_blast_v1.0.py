#!/usr/bin/python3

import argparse
import subprocess

version = "v1.0"
release_date = "16-09-29"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
description = "Perform a basic reciprocal BLAST with no extra parsing."
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n{}".format(author, release_date, version, description)

MAKE_BLAST_DB_COMMAND = 'makeblastdb -in {in_fa} -parse_seqids -dbtype {type}'
BLAST_COMMAND = '{blast_type} -query {query} -db {db} -num_threads {threads} -max_target_seqs 1 -outfmt 6'


def main():

    args = parse_arguments()

    print('{} cores assigned for processing'.format(args.threads))

    make_blast_db(args.in1, args.type)
    make_blast_db(args.in2, args.type)

    run_blast(args.in1, args.in2, args.out1, args.type, args.threads)
    run_blast(args.in2, args.in1, args.out2, args.type, args.threads)


def make_blast_db(input_fp, db_type):

    """Create BLAST database from provided FASTA"""

    command = MAKE_BLAST_DB_COMMAND.format(in_fa=input_fp, type=db_type)
    run_command(command)


def run_blast(input_fp, database_fp, output_fp, db_type, threads=1):

    """Perform a BLAST on provided input fasta"""

    # BLAST_COMMAND = '{blast_type} -query {query} -db {db} -max_target_seqs 1 -outfmt 6'

    if db_type == 'prot':
        blast_type = 'blastp'
    elif db_type == 'nucl':
        blast_type = 'blastn'
    else:
        raise ValueError('db_type "{}" not covered'.format(db_type))

    command = BLAST_COMMAND.format(blast_type=blast_type,
                                   query=input_fp,
                                   db=database_fp,
                                   threads=threads)
    run_command(command, stdout_path=output_fp)


def run_command(command, stdout_path=None):

    """Wrapper util to use when running commands"""
    print('>>> Running the command: {}'.format(command))

    if not stdout_path:
        subprocess.call(command.split(' '))
    else:
        print('Writing output to : {}'.format(stdout_path))
        with open(stdout_path, 'w') as out_fh:
            subprocess.call(command.split(' '), stdout=out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--in1', help='First of the two FASTA files', required=True)
    parser.add_argument('--in2', help='Second of the two FASTA files', required=True)
    parser.add_argument('--type', choices=['prot', 'nucl'],
                        help='Is the input peptides or nucleotides', required=True)
    parser.add_argument('--out1', help='Blast1 output', required=True)
    parser.add_argument('--out2', help='Blast2 output', required=True)
    parser.add_argument('--threads', help='Number of threads for BLAST processing', default=1)

    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()


