__author__ = 'jakob'

import SAM_filtering_utils as util
import gzip


def filter_out_correctly_labelled_reads(primary_sam_fp, filter_hits_fp, filter_remove_fp, target_patterns, report_fp=None):

    """
    Iterates through the primary SAM file
    Reads matching provided pattern in column 3 (reference sequence name)
    Returns set containing the matched IDs
    """

    nbr_header_lines = 0
    nbr_matched_lines_dict = dict()
    nbr_removed_lines = 0
    matched_read_ids_dict = dict()
    non_matched_lines = set()

    for pattern in target_patterns:
        matched_read_ids_dict[pattern] = set()
        nbr_matched_lines_dict[pattern] = 0

    with open(primary_sam_fp) as primary_sam_fh, \
            open(filter_hits_fp, 'w') as filter_hits_fh, \
            open(filter_remove_fp, 'w') as filter_remove_fh:

        line_number = 0
        for line in primary_sam_fh:
            line = line.rstrip()
            line_number += 1

            if line.startswith('@'):  # SAM headers
                print(line, file=filter_hits_fh)
                print(line, file=filter_remove_fh)
                nbr_header_lines += 1
            else:
                label_col = line.split('\t')[2]

                found_match = parse_alignment_line(line, filter_hits_fh, target_patterns, nbr_matched_lines_dict,
                                                   matched_read_ids_dict, label_col)

                if not found_match:
                    non_matched_lines.add(line)
                    print(line, file=filter_remove_fh)
                    nbr_removed_lines += 1

    output_report_(report_fp, matched_read_ids_dict, nbr_header_lines, nbr_matched_lines_dict, nbr_removed_lines)

    return matched_read_ids_dict, non_matched_lines


def parse_alignment_line(line, filter_hits_fh, target_patterns, nbr_matched_lines, matched_read_ids_dict, label_col):

    """Check if SAM line matches target pattern, return true if it found any match"""

    found_match = False
    for pattern in target_patterns:

        if label_col.find(pattern) != -1:
            print(line, file=filter_hits_fh)
            nbr_matched_lines[pattern] += 1

            read_id = line.split('\t')[0]
            matched_read_ids_dict[pattern].add(read_id)

            found_match = True
            break

    return found_match


def output_report_(report_fp, matched_read_ids_dict, nbr_header_lines, nbr_matched_lines_dict, nbr_removed_lines):

    """Write reports containing statistics from the filtering"""

    total_lines = 0
    total_lines += nbr_removed_lines
    for pattern in nbr_matched_lines_dict:
        total_lines += nbr_matched_lines_dict[pattern]

    with open(report_fp, 'w') as report_fh:

        print('Header lines:\t{}'.format(nbr_header_lines), file=report_fh)
        print('Non-matching lines:\t{}'.format(nbr_removed_lines), file=report_fh)

        for pattern in nbr_matched_lines_dict:

            print('----- Pattern: {} -----'.format(pattern), file=report_fh)

            match_count = nbr_matched_lines_dict[pattern]
            print('Matching lines for {}:\t{}'.format(pattern, nbr_matched_lines_dict[pattern]), file=report_fh)
            fraction = match_count / total_lines
            print('Fraction matching:\t{}'.format(fraction), file=report_fh)

            matched_read_ids_set = set(matched_read_ids_dict[pattern])
            paired_end_count = len(matched_read_ids_dict[pattern]) - len(matched_read_ids_set)
            single_end_count = len(matched_read_ids_set) - paired_end_count
            print('Paired end reads:\t{}'.format(paired_end_count), file=report_fh)
            print('Single end reads:\t{}'.format(single_end_count), file=report_fh)

    return matched_read_ids_dict


def output_filtered_fastq(args, matched_read_ids_dict, output_directory):

    """Output filtered fastq file(s) based on matched read IDs from SAM/BAM files"""

    read_files = args.reads_fq.split(',')
    print('{} read files found: {}'.format(len(read_files), ','.join(read_files)))

    for read_file in read_files:

        if read_file.endswith('.gz'):
            single_in_fh = gzip.open(read_file, 'rt')
        else:
            single_in_fh = open(read_file, 'r')

        out_fh_dict = dict()
        for pattern in matched_read_ids_dict:

            fq_out_fp = output_directory + util.insert_suffix(read_file, 'matching_' + pattern, cut_off_path=True)

            if fq_out_fp.endswith('.gz'):
                out_fh_dict[pattern] = gzip.open(fq_out_fp, 'wt')
            else:
                out_fh_dict[pattern] = open(fq_out_fp, 'w')

        filter_and_write_file(matched_read_ids_dict, single_in_fh, out_fh_dict)

        single_in_fh.close()
        for out_fh in out_fh_dict.values():
            out_fh.close()


def filter_and_write_file(matched_read_ids_dict, single_in_fh, out_fh_dict):

    """Output fastq reads which has alignments matching to target pattern"""

    for header_line in single_in_fh:
        header_line = header_line.rstrip()
        seq_line = single_in_fh.readline().rstrip()
        qual_header_line = single_in_fh.readline().rstrip()
        qual_line = single_in_fh.readline().rstrip()

        header_id = header_line[1:].split(' ')[0]

        for pattern in matched_read_ids_dict.keys():

            if header_id in matched_read_ids_dict[pattern]:
                print(header_line, file=out_fh_dict[pattern])
                print(seq_line, file=out_fh_dict[pattern])
                print(qual_header_line, file=out_fh_dict[pattern])
                print(qual_line, file=out_fh_dict[pattern])
