__author__ = 'jakob'

import datetime
import os
import subprocess


def check_input_files(in_file, args):

    """Verify that input SAM/BAM file is of correct format"""

    if args.in_format == 'SAM':
        assert in_file.endswith('.sam'), print('Input file must have .sam suffix!')
    elif args.in_format == 'BAM':
        assert in_file.endswith('.bam'), print('Input file must have .bam suffix!')
    else:
        print('Unknown chosen input format!')
        exit(1)

    allowed_fq_endings = ['.fq', '.fastq', '.fq.gz', '.fastq.gz']
    if args.reads_fq:

        fw_reads, rv_reads = args.reads_fq.split(',')
        matched_suffix = False

        for ending in allowed_fq_endings:
            if fw_reads.endswith(ending) and rv_reads.endswith(ending):
                matched_suffix = True

        assert matched_suffix, print("Fastq files didn't match any allowed suffix: {}"
                                     .format(','.join(allowed_fq_endings)))


def get_output_directory_path(in_file, args):

    """Return complete output directory path"""

    if args.out_dir:
        return args.out_dir + '/'
    else:
        return '/'.join(os.path.abspath(in_file).split('/')[0:-1]) + '/'


def change_suffix(initial_value, new_suffix, initial_suffix=None, cut_off_path=False):

    """Replaces the suffix with a new provided suffix"""

    working_value = initial_value

    if cut_off_path:
        working_value = working_value.split('/')[-1]

    if initial_suffix:
        assert initial_value.endswith(initial_suffix), \
            print("The starting suffix must be: {}, current starting string: {}".format(initial_suffix, working_value))

    return '.'.join(working_value.split('.')[0:-1]) + '.' + new_suffix


def insert_suffix(initial_value, inserted_suffix, cut_off_path=False):

    """Insert word before final suffix in path"""

    working_value = initial_value

    if cut_off_path:
        working_value = working_value.split('/')[-1]

    filename_parts = working_value.split('.')
    filename_parts.insert(-1, inserted_suffix)

    return '.'.join(filename_parts)


def timestamp():

    """Use datetime module to return a timestamp-string"""

    return str(datetime.datetime.now()).split('.')[0]


def run_bash(command, stdout_path=None):

    """Generic function for executing bash commands via subprocess module"""

    command_list = command.split(' ')
    print('Running Bash command: {}'.format(' '.join(command_list)))
    if stdout_path:
        with open(stdout_path, 'w') as out_fh:
            subprocess.call(command_list, stdout=out_fh)
    else:
        subprocess.call(command_list)


def clean_out_temporary_files(temporary_files):

    """Removes provided list of files from system"""

    print('Temporary files to be removed: {}'.format(len(temporary_files)))

    for temp_file in temporary_files:
        print('Removing temporary file: {}'.format(temp_file))
        os.remove(temp_file)

    print('Cleanup done')
