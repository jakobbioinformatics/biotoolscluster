#!/usr/bin/python3

import argparse
import SAM_filtering_utils as util
import SAM_filtering_runsteps as runsteps

version = 'v2.4'
release_date = '16-08-16'
author = 'Jakob Willforss (jakob.willforss@hotmail.com)'
description = 'This script extracts primary alignments from SAM files matching particular IDs.\n' \
              'Consequently, it can be used to separate SAM entries best matching for example different genomes\n' \
              '\n' \
              '--- Processing flow ---\n' \
              '(1) Convert input SAM to BAM format\n' \
              '(2) Sort BAM file on name (required for processing paired reads in for example HTSeq-count)\n' \
              '(3) Extract primary reads and convert to SAM format\n' \
              '(4) Separate the primary SAM reads to output files with read IDs matching and not matching the pattern\n'

help_message = 'Author: {}\nRelease date: {}\nVersion: {}\n\n{}'.format(author, release_date, version, description)

SAM_TO_BAM_STDOUT_COMMAND = 'samtools view -@ {threads} -S -b {in_sam}'
SORT_BAM_ON_NAME_STDOUT_COMMAND = 'samtools sort -@ {threads} -m {thread_mem} -n -o {unsorted_bam} {bam_sort_tmp}'
GET_PRIMARY_SAM_STDOUT_COMMAND = 'samtools view -@ {threads} -F 256 -h {sorted_bam}'


def main():

    # >>> Setup <<< #

    args = parse_arguments()

    tc = args.threads
    in_file = args.in_file
    max_memory = args.max_memory
    thread_mem = int(max_memory / tc)

    util.check_input_files(in_file, args)
    out_dir = util.get_output_directory_path(in_file, args)
    print('Assigned output directory: {}'.format(out_dir))
    report_file = out_dir + 'report.txt'

    if args.in_format == 'SAM':
        initial_bam = out_dir + util.change_suffix(in_file, 'bam', initial_suffix='sam', cut_off_path=True)
    else:
        initial_bam = args.in_file

    initial_suffix = args.in_format.lower()
    bam_sort_tmp = out_dir + 'bam_sort_tmp'
    sorted_bam = out_dir + util.change_suffix(in_file, 'sorted.bam', initial_suffix=initial_suffix, cut_off_path=True)
    primary_sam = out_dir + util.change_suffix(in_file, 'primary.sam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_hits_sam = out_dir + util.change_suffix(in_file, 'matched.sam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_remove_sam = out_dir + util.change_suffix(in_file, 'non_matched.sam', initial_suffix=initial_suffix, cut_off_path=True)

    primary_bam = out_dir + util.change_suffix(in_file, 'primary.bam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_hits_bam = out_dir + util.change_suffix(in_file, 'matched.bam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_remove_bam = out_dir + util.change_suffix(in_file, 'non_matched.bam', initial_suffix=initial_suffix, cut_off_path=True)

    temporary_files = [primary_sam, filter_hits_sam, filter_remove_sam]

    print('Output paths:\n{}\n{}\n{}\n{}\n{}'.format(initial_bam, sorted_bam, primary_sam, filter_hits_sam,
                                                     filter_remove_sam))

    # >>> Run <<< #

    if args.in_format == 'SAM':
        print(util.timestamp() + ' - Converting input SAM to BAM file: {}'.format(initial_bam))
        util.run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=in_file), initial_bam)

    if args.sort_on_name:
        print(util.timestamp() + ' - Sorting BAM file on name to:'.format(sorted_bam))
        util.run_bash(SORT_BAM_ON_NAME_STDOUT_COMMAND.format(threads=tc,
                                                             thread_mem=str(thread_mem) + 'M',
                                                             unsorted_bam=initial_bam,
                                                             bam_sort_tmp=bam_sort_tmp), sorted_bam)
    else:
        sorted_bam = initial_bam

    print(util.timestamp() + ' - Extracting primary reads in SAM format from sorted BAM to: {}'.format(primary_sam))
    util.run_bash(GET_PRIMARY_SAM_STDOUT_COMMAND.format(threads=tc, sorted_bam=sorted_bam), primary_sam)

    print(util.timestamp() + ' - Filters primary SAM file to hits: {} and non-hits: {}'.format(filter_hits_sam,
                                                                                               filter_remove_sam))
    matched_read_ids_dict, non_matched_lines = runsteps.filter_out_correctly_labelled_reads(primary_sam,
                                                                                            filter_hits_sam,
                                                                                            filter_remove_sam,
                                                                                            args.target_patterns.split(','),
                                                                                            report_file)

    if args.compress:
        print(util.timestamp() + ' - Compressing sam files to bam format')
        util.run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=primary_sam), primary_bam)
        util.run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=filter_hits_sam), filter_hits_bam)

        if len(non_matched_lines):
            util.run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=filter_remove_sam), filter_remove_bam)

    print(util.timestamp() + ' - SAM/BAM filtering all done!')

    # >>> Filter fastq <<< #

    if args.reads_fq:
        print(util.timestamp() + ' - Fastq samples present: {}, filtering...'.format(args.reads_fq))
        runsteps.output_filtered_fastq(args, matched_read_ids_dict, out_dir)
        print(util.timestamp() + ' - Fastq filtering all done!')

    if not args.save_all:
        util.clean_out_temporary_files(temporary_files)


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--in_file', help='Input file (either SAM or BAM)', required=True)
    parser.add_argument('--in_format', help='Input in SAM or BAM format', choices=['SAM', 'BAM'], default='SAM')

    parser.add_argument('--target_patterns',
                        help='Filter ID used to distinguish desired SAM entries.'
                             'IDs matching patterns are retained in SAM/BAM output file, '
                             'but separated in FASTQ output file.',
                        required=True)
    parser.add_argument('--out_dir', help='If applied, the output is written here. Otherwise, files are produced in '
                                          'the same folder as the input file')
    parser.add_argument('--threads', help='Number of threads used by Samtools', default=1, type=int)
    parser.add_argument('--max_memory', help='Max BAM sorting memory in MBs', type=int, default=2000)

    parser.add_argument('--sort_on_name', help='If SAM/BAM is not sorted on name, perform sorting', action='store_true')
    parser.add_argument('--compress', help='Convert SAM files to BAM format', action='store_true')
    parser.add_argument('--save_all', help='Do not clean out temporary files', action='store_true')

    parser.add_argument('--reads_fq', help='Input reads in fastq format for filtering'
                                           'Paired reads are separated by a comma: sample_fw,sample_rv')

    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    main()
