#!/usr/bin/python3

version = 'v2.0'
release_date = '16-01-06'
author = 'Jakob Willforss (jakob.willforss@hotmail.com)'
description = 'This script extracts primary alignments from SAM files matching particular IDs.\n' \
              'Consequently, it can be used to separate SAM entries best matching for example different genomes\n' \
              '\n' \
              '--- Processing flow ---\n' \
              '(1) Convert input SAM to BAM format\n' \
              '(2) Sort BAM file on name (required for processing paired reads in for example HTSeq-count)\n' \
              '(3) Extract primary reads and convert to SAM format\n' \
              '(4) Separate the primary SAM reads to output files with read IDs matching and not matching the pattern\n'

help_message = 'Author: {}\nRelease date: {}\nVersion: {}\n\n{}'.format(author, release_date, version, description)

import argparse
import subprocess
import os
import datetime

SAM_TO_BAM_STDOUT_COMMAND = 'samtools view -@ {threads} -S -b {in_sam}'
SORT_BAM_ON_NAME_STDOUT_COMMAND = 'samtools sort -@ {threads} -m {thread_mem} -n -o {unsorted_bam} {bam_sort_tmp}'
GET_PRIMARY_SAM_STDOUT_COMMAND = 'samtools view -@ {threads} -F 256 -h {sorted_bam}'


def main():

    # Setup

    args = parse_arguments()

    tc = args.threads
    in_file = args.in_file
    max_memory = args.max_memory
    thread_mem = int(max_memory / tc)

    check_input_file(in_file, args)
    output_directory = get_output_directory_path(in_file, args)
    print('Assigned output directory: {}'.format(output_directory))
    report_file = output_directory + 'report.txt'

    if args.in_format == 'SAM':
        initial_bam = output_directory + change_suffix(in_file, 'bam', initial_suffix='sam', cut_off_path=True)
    else:
        initial_bam = args.in_file

    initial_suffix = args.in_format.lower()
    bam_sort_tmp = output_directory + 'bam_sort_tmp'
    sorted_bam = output_directory + change_suffix(in_file, 'sorted.bam', initial_suffix=initial_suffix, cut_off_path=True)
    primary_sam = output_directory + change_suffix(in_file, 'primary.sam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_hits_sam = output_directory + change_suffix(in_file, 'matched.sam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_remove_sam = output_directory + change_suffix(in_file, 'non_matched.sam', initial_suffix=initial_suffix, cut_off_path=True)

    primary_bam = output_directory + change_suffix(in_file, 'primary.bam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_hits_bam = output_directory + change_suffix(in_file, 'matched.bam', initial_suffix=initial_suffix, cut_off_path=True)
    filter_remove_bam = output_directory + change_suffix(in_file, 'non_matched.bam', initial_suffix=initial_suffix, cut_off_path=True)

    print('Output paths:\n{}\n{}\n{}\n{}\n{}'.format(initial_bam, sorted_bam, primary_sam, filter_hits_sam,
                                                     filter_remove_sam))

    # Run

    if args.in_format == 'SAM':
        print(timestamp() + ' - Converting input SAM to BAM file: {}'.format(initial_bam))
        run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=in_file), initial_bam)

    print(timestamp() + ' - Sorting BAM file on name to:'.format(sorted_bam))
    run_bash(SORT_BAM_ON_NAME_STDOUT_COMMAND.format(threads=tc, thread_mem=str(thread_mem) + 'M', unsorted_bam=initial_bam,
                                                    bam_sort_tmp=bam_sort_tmp), sorted_bam)

    print(timestamp() + ' - Extracting primary reads in SAM format from sorted BAM to: {}'.format(primary_sam))
    run_bash(GET_PRIMARY_SAM_STDOUT_COMMAND.format(threads=tc, sorted_bam=sorted_bam), primary_sam)

    print(timestamp() + ' - Filters primary SAM file to hits: {} and non-hits: {}'.format(filter_hits_sam, filter_remove_sam))
    matched_read_ids = filter_out_correctly_labelled_reads(primary_sam, filter_hits_sam, filter_remove_sam, args.target_pattern, report_file)

    if args.compress:
        print(timestamp() + ' - Compressing sam files to bam format')
        run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=primary_sam), primary_bam)
        run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=filter_hits_sam), filter_hits_bam)
        run_bash(SAM_TO_BAM_STDOUT_COMMAND.format(threads=tc, in_sam=filter_remove_sam), filter_remove_bam)

    print(timestamp() + ' - SAM/BAM filtering all done!')

    # Filter fastq

    if args.reads_fq:
        print(timestamp() + ' - Fastq samples present: {}, filtering...'.format(args.reads_fq))
        output_filtered_fastq(args, matched_read_ids, output_directory)
        print(timestamp() + ' - Fastq filtering all done!')



def check_input_file(in_file, args):

    """Verify that input SAM/BAM file is of correct format"""

    if args.in_format == 'SAM':
        assert in_file.endswith('.sam'), print('Input file must have .sam suffix!')
    elif args.in_format == 'BAM':
        assert in_file.endswith('.bam'), print('Input file must have .bam suffix!')
    else:
        print('Unknown chosen input format!')
        exit(1)


def get_output_directory_path(in_file, args):

    """Return complete output directory path"""

    if args.out_dir:
        return args.out_dir + '/'
    else:
        return '/'.join(os.path.abspath(in_file).split('/')[0:-1]) + '/'


def timestamp():

    """Use datetime module to return a timestamp-string"""

    return str(datetime.datetime.now()).split('.')[0]


def change_suffix(initial_value, new_suffix, initial_suffix=None, cut_off_path=False):

    """Replaces the suffix with a new provided suffix"""

    working_value = initial_value

    if cut_off_path:
        working_value = working_value.split('/')[-1]

    if initial_suffix:
        assert initial_value.endswith(initial_suffix), \
            print("The starting suffix must be: {}, current starting string: {}".format(initial_suffix, working_value))

    return '.'.join(working_value.split('.')[0:-1]) + '.' + new_suffix


def filter_out_correctly_labelled_reads(primary_sam_fp, filter_hits_fp, filter_remove_fp, target_pattern, report_fp=None):

    """
    Iterates through the primary SAM file
    Reads matching provided pattern in column 3 (reference sequence name)
    Returns set containing the matched IDs
    """

    nbr_header_lines = 0
    nbr_matched_lines = 0
    nbr_removed_lines = 0

    matched_read_ids = list()

    with open(primary_sam_fp) as primary_sam_fh, \
            open(filter_hits_fp, 'w') as filter_hits_fh, \
            open(filter_remove_fp, 'w') as filter_remove_fh:

        for line in primary_sam_fh:
            line = line.rstrip()

            if line.startswith('@'):
                print(line, file=filter_hits_fh)
                print(line, file=filter_remove_fh)
                nbr_header_lines += 1
            else:
                label_col = line.split('\t')[2]

                if label_col.find(target_pattern) != -1:
                    print(line, file=filter_hits_fh)
                    nbr_matched_lines += 1

                    read_id = line.split('\t')[0]
                    matched_read_ids.append(read_id)
                else:
                    print(line, file=filter_remove_fh)
                    nbr_removed_lines += 1

    matched_read_ids_set = set(matched_read_ids)
    paired_end_count = len(matched_read_ids) - len(matched_read_ids_set)
    single_end_count = len(matched_read_ids_set) - paired_end_count
    with open(report_fp, 'w') as report_fh:
        print('Header lines:\t{}'.format(nbr_header_lines), file=report_fh)
        print('Matching lines:\t{}'.format(nbr_matched_lines), file=report_fh)
        print('Non-matching lines:\t{}'.format(nbr_removed_lines), file=report_fh)
        print('Fraction matching:\t{}'.format(nbr_matched_lines / (nbr_matched_lines + nbr_removed_lines)), file=report_fh)
        print('Paired end reads:\t{}'.format(paired_end_count), file=report_fh)
        print('Single end reads:\t{}'.format(single_end_count), file=report_fh)

    return matched_read_ids_set


def run_bash(command, stdout_path=None):

    """Generic function for executing bash commands via subprocess module"""

    command_list = command.split(' ')
    print('Running Bash command: {}'.format(' '.join(command_list)))
    if stdout_path:
        with open(stdout_path, 'w') as out_fh:
            subprocess.call(command_list, stdout=out_fh)
    else:
        subprocess.call(command_list)


def clean_out_temporary_files(temporary_files):

    """Removes provided list of files from system"""

    for temp_file in temporary_files:
        os.remove(temp_file)


def output_filtered_fastq(args, matched_read_ids, output_directory):

    """Output filtered fastq file(s) based on matched read IDs from SAM/BAM files"""

    read_files = args.reads_fq.split(',')
    print('{} read files found: {}'.format(len(read_files), read_files))

    header_ids_temp = list()

    for read_file in read_files:

        fq_out_fp = output_directory + change_suffix(read_file, 'matching.fastq', cut_off_path=True)
        with open(read_file) as single_in_fh, open(fq_out_fp, 'w') as fq_out_fh:

            for header_line in single_in_fh:
                header_line = header_line.rstrip()
                seq_line = single_in_fh.readline().rstrip()
                qual_header_line = single_in_fh.readline().rstrip()
                qual_line = single_in_fh.readline().rstrip()

                header_id = header_line[1:].split(' ')[0]
                
                header_ids_temp.append(header_id)

                if header_id in matched_read_ids:
                    print(header_line, file=fq_out_fh)
                    print(seq_line, file=fq_out_fh)
                    print(qual_header_line, file=fq_out_fh)
                    print(qual_line, file=fq_out_fh)


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--in_file', help='Input file (either SAM or BAM)', required=True)
    parser.add_argument('--in_format', help='Input in SAM or BAM format', choices=['SAM', 'BAM'], default='SAM')

    parser.add_argument('--target_pattern', help='Filter ID used to distinguish desired SAM entries', required=True)
    parser.add_argument('--out_dir', help='If applied, the output is written here. Otherwise, files are produced in '
                                          'the same folder as the input file')
    parser.add_argument('--threads', help='Number of threads used by Samtools', default=1, type=int)
    parser.add_argument('--max_memory', help='Max BAM sorting memory in MBs', type=int, default=2000)

    parser.add_argument('--compress', help='Convert SAM files to BAM format', action='store_true')

    parser.add_argument('--reads_fq', help='Input reads in fastq format for filtering'
                                           'Paired reads are separated by a comma: sample_fw,sample_rv')

    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    main()
