#!/usr/bin/python3

version = "v1.0.0"
release_date = "16-02-23"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
description = "This script reads TransDecoder output and extracts the longest found open reading frames for each transcript."
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n{}".format(author, release_date, version, description)

import argparse
from Bio import SeqIO
from Bio.SeqIO import FastaIO

def main():

    args = parse_arguments()

    fasta_entries_dict = get_fasta_entries(args.transdecoder_fasta)
    longest_records = get_longest_records(fasta_entries_dict)
    output_records(args.output_fasta, longest_records)


def get_fasta_entries(fasta_path):

    """
    Read all TransDecoder fasta entries into a dictionary
    Entries are grouped on base-name (before TransDecoder ORF identification) in a dictionary
    This dictionary is returned
    """

    fasta_entries = dict()

    with open(fasta_path, 'r') as in_fh:
        for record in SeqIO.parse(in_fh, 'fasta'):

            record_id = record.id
            original_id = record_id.split('|')[0]

            if fasta_entries.get(original_id):
                fasta_entries[original_id].append(record)
            else:
                fasta_entries[original_id] = [record]

    return fasta_entries


def get_longest_records(fasta_entries_dict):

    """Based on records grouped on base id in dict, the longst records are extracted and returned"""

    longest_records = list()
    for original_id in sorted(fasta_entries_dict):

        longest_rec = fasta_entries_dict[original_id][0]

        for record in fasta_entries_dict[original_id]:
            if len(record.seq) > len(longest_rec.seq):
                longest_rec = record

        longest_records.append(longest_rec)

    return longest_records


def output_records(output_fp, records):

    """Writes a list of fasta records to file"""

    print(records[0])

    with open(output_fp, 'w') as out_fh:
        fasta_writer = FastaIO.FastaWriter(out_fh, wrap=None)
        fasta_writer.write_file(records)


def parse_arguments():

    parser = argparse.ArgumentParser(description=help_message, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--transdecoder_fasta', help='Input fasta using TransDecoder headers', required=True)
    parser.add_argument('--output_fasta', help='Reduced fasta file with shorter ORFs removed', required=True)
    parser.add_argument('--version', action='version', version=version)
    args = parser.parse_args()
    return args

class FastaEntry:

    def __init__(self, header, sequence):
        self.header = header
        self.sequence = sequence

    def __str__(self):
        return '>{}\n{}'.format(self.header, self.sequence)

if __name__ == '__main__':
    main()


