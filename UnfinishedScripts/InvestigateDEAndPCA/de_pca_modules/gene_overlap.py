__author__ = 'jakob'

import os

def read_pca_gene_list_dir(pca_gene_list_dir):

    component_data_dict = dict()

    for gene_list_file in os.listdir(pca_gene_list_dir):
        print('Found: {}'.format(gene_list_file))

        is_header = True

        with open(gene_list_file) as in_fh:

            gene_list = list()
            for line in in_fh:
                line = line.rstrip()

                if is_header:
                    is_header = False
                    continue

                number, gene_id, contribution = line.split()
                gene_list.append(gene_id)

        component_data_dict[gene_list_file] = gene_list

    return component_data_dict


def read_expression_matrices(expression_matrix_string, delimiter=','):

    expression_matrices_paths = expression_matrix_string.split(',')

    print('Loaded expression matrices: {}'.format(expression_matrices_paths))

    for expression_matrix_file in expression_matrices_paths:

        de_genes_dict = dict()

        with open(expression_matrix_file) as in_fh:

            is_header = True

            for line in in_fh:
                line = line.rstrip()

                if is_header:
                    is_header = False
                    continue

                line_fields = line.split(delimiter)










