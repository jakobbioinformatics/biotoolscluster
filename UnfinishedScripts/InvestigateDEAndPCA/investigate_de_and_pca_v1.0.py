#!/usr/bin/python3

version = "v1.0"
release_date = "16-04-28"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "Utilities for investigating PCAs related to differential expression." \
    .format(author, release_date, version)

import argparse

import de_pca_modules.gene_overlap as gene_overlap


def gene_overlap_main(args):

    gene_overlap.read_pca_gene_list_dir(args.pca_gene_list_dir)
    gene_overlap.read_expression_matrices(args.expression_matrices)


def parse_arguments():

    def default_func(args):
        print("Must specify a sub-parser, exitting...")
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: gene_overlap')

    blast_parser = subparsers.add_parser('gene_overlap')
    blast_parser.set_defaults(func=gene_overlap_main)
    blast_parser.add_argument('--pca_gene_list_dir',
                              help='Directory containing files for genes contributing most to PCs',
                              required=True)
    blast_parser.add_argument('--expression_matrices',
                              help='Expression matrices to run comparisons against (comma-delimited string)',
                              required=True)
    blast_parser.add_argument('--output_matrix',
                              help='CSV-delimited matrix containing number of overlapping genes',
                              required=True)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    parse_arguments()
