#!/usr/bin/env python3

import argparse
import os
import sys


def load_module(modname, modpath):
    if sys.version_info.minor >= 5:
        import importlib.util
        spec = importlib.util.spec_from_file_location(modname, modpath)
        module_exec = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module_exec)
        return module_exec
    else:
        from importlib.machinery import SourceFileLoader
        return SourceFileLoader(modname, modpath).load_module()


syspath = os.path.dirname(os.path.realpath(__file__))

translate_ids_modname = "jwtk_modules.mod_translate_ids"
translate_ids_modpath = syspath + "/jwtk_modules/mod_translate_ids.py"
mod_translate_fasta_ids = load_module(translate_ids_modname, translate_ids_modpath)

fasta_stats_modname = "jwtk_modules.mod_fasta_stats"
fasta_stats_modpath = syspath + "/jwtk_modules/mod_fasta_stats.py"
mod_fasta_stats = load_module(fasta_stats_modname, fasta_stats_modpath)


version = "v1.3"
release_date = "16-09-23"
author = "Jakob Willforss (jakob.willforss@hotmail.com)"
description = "Toolkit for file processing not provided by the tool(s): \n\n- seqtk"
help_message = "Author: {}\nRelease date: {}\nVersion: {}\n\n" \
               "{}" \
    .format(author, release_date, version, description)


def parse_arguments():

    def default_func(args):
        print("Must specify tool (jwtk <tool>)")
        parser.print_help()
        exit(1)

    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)

    subparsers = parser.add_subparsers(help='Commands: translate_ids fs')
    parse_translate_fasta_ids(subparsers)
    parse_fasta_stats(subparsers)

    args = parser.parse_args()
    args.func(args)


def parse_fasta_stats(subparsers_object):

    def fasta_stats(args):
        mod_fasta_stats.main(args.fasta)

    subparser = subparsers_object.add_parser('fs')
    subparser.set_defaults(func=fasta_stats)

    subparser.add_argument('fasta', help='FASTA file for stats calculation')


def parse_translate_fasta_ids(subparsers_object):

    def translate_fasta_ids(args):
        mod_translate_fasta_ids.main(args.input,
                                     args.id_map,
                                     reverse_mapping=args.reverse_mapping,
                                     output_fp=args.output,
                                     trim=args.trim_chars,
                                     file_format=args.input_type,
                                     delim=args.delim,
                                     force_missing=args.force_missing)

    subparser = subparsers_object.add_parser('translate_ids')
    subparser.set_defaults(func=translate_fasta_ids)

    accepted_formats = mod_translate_fasta_ids.ACCEPTED_FORMATS

    subparser.add_argument('--input', help='FASTA with IDs to rename (- to read from STDIN)', required=True)
    subparser.add_argument('--id_map', help='Two column ID map file (tsv)', required=True)
    subparser.add_argument('--reverse_mapping', action='store_true')
    subparser.add_argument('--output', help='Direct output to target file instead of STDOUT')
    subparser.add_argument('--trim_chars', help='Trim characters from the input tools')
    subparser.add_argument('--input_type', help='Input file format', choices=accepted_formats, required=True)
    subparser.add_argument('--delim', help='Type of delimitor for tables', default='\t')
    subparser.add_argument('--force_missing', help='Continue with warning if ID not present in map', action='store_true')


if __name__ == '__main__':
    parse_arguments()
