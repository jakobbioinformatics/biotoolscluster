from Bio import SeqIO


def main(fasta_fp):

    fasta_count = count_entries(fasta_fp)
    n50_length = calculate_n50(fasta_fp)

    print('Contigs:\t{}'.format(fasta_count))

    print('N50:\t\t{}'.format(n50_length))
    print('Warning! This N50 value does not strictly adhere to the guidelines presented here: ')
    print('https://www.broad.harvard.edu/crd/wiki/index.php/N50')
    print('(Median values are not averaged)')


def calculate_n50(fasta_fp):

    """Calculate N50 for target FASTA"""

    all_lengths = list()

    with open(fasta_fp) as in_fh:
        for record in SeqIO.parse(in_fh, "fasta"):
            all_lengths.append(len(record))

    all_lengths.sort()
    total_length = sum(all_lengths)
    n50_threshold = total_length / 2

    length_sum = 0
    for length in all_lengths:
        length_sum += length

        if length_sum >= n50_threshold:
            return length


def count_entries(fasta_fp):

    """Count entries in FASTA"""

    entries = 0

    with open(fasta_fp) as in_fh:
        for _ in SeqIO.parse(in_fh, "fasta"):
            entries += 1

    return entries
