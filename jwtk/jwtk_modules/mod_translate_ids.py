import sys


ACCEPTED_FORMATS = ['fasta', 'head_table', 'table']
MAP_DELIM = '\t'


def main(input_fp, id_map_fp, reverse_mapping=False, output_fp=None, delim='\t',
         file_format='fasta', trim=None, force_missing=False):

    id_mapping_dict = get_id_mapping_dict(id_map_fp, MAP_DELIM, reverse_mapping=reverse_mapping)

    in_fh = get_in_fh(input_fp)
    out_fh = get_out_fh(output_fp)

    if file_format == 'fasta':
        output_formatted_fasta(in_fh, out_fh, id_mapping_dict, force_missing=force_missing)
    elif file_format == 'head_table':
        output_formatted_table(in_fh, out_fh, id_mapping_dict, delim, has_header=True, 
                               trim_chars=trim, force_missing=force_missing)
    elif file_format == 'table':
        output_formatted_table(in_fh, out_fh, id_mapping_dict, delim, has_header=False, 
                               trim_chars=trim, force_missing=args.force_missing)
    else:
        print('Unknown file format - Currently accepted are: {}'.format(' '.join(ACCEPTED_FORMATS)))

    in_fh.close()
    out_fh.close()


def get_in_fh(in_fp):

    if in_fp != '-':
        in_fh = open(in_fp)
    else:
        in_fh = sys.stdin

    return in_fh


def get_out_fh(out_fp):

    if out_fp:
        out_fh = open(out_fp, 'w')
    else:
        out_fh = sys.stdout

    return out_fh


def output_formatted_fasta(in_fh, out_fh, id_mapping_dict, force_missing=False):

    for line in in_fh:
        line = line.rstrip()

        if line.startswith('>'):
            current_id = line[1:]

            if id_mapping_dict.get(current_id) is None:
                print("ID not found: {}".format(current_id))
                if not force_missing:
                    print("Stopping")
                    sys.exit(1)
                else:
                    continue

            new_id = id_mapping_dict[current_id]

            print('>{}'.format(new_id), file=out_fh)
        else:
            print(line, file=out_fh)


def output_formatted_table(in_fh, out_fh, id_mapping_dict, delim, has_header=False, trim_chars=None, force_missing=False):

    skip_next = has_header

    for line in in_fh:
        line = line.rstrip()

        if skip_next:
            print(line, file=out_fh)
            skip_next = False
            continue

        line_fields = line.split(delim)
        name_field = line_fields[0]

        if trim_chars:
            name_field = name_field.strip(trim_chars)

        if id_mapping_dict.get(name_field) is None:
            print("!ID not found: {}".format(name_field))
            if not force_missing:
                print("Stopping")
                sys.exit(1)
            else:
                continue

        new_name = id_mapping_dict[name_field]

        new_line = '{new_name}{delim}{fields}'.format(new_name=new_name,
                                                      delim=delim,
                                                      fields=delim.join(line_fields[1:]))

        print(new_line, file=out_fh)


def get_id_mapping_dict(id_map_fp, delim, reverse_mapping=False):

    """Retrieve mapping dict from two-column ID map format"""

    id_mapping_dict = dict()

    with open(id_map_fp) as in_fh:
        for line in in_fh:
            line = line.rstrip()

            left_id, right_id = line.split(delim)

            if not reverse_mapping:
                id_mapping_dict[left_id] = right_id
            else:
                id_mapping_dict[right_id] = left_id

    return id_mapping_dict
